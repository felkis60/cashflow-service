module gitlab.com/felkis60/cashflow-service

go 1.17

require (
	gitlab.com/felkis60/rex-microservices-helpers v0.0.0-20221123122256-d4cfe93d1aeb
	gorm.io/gorm v1.24.2
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/goccy/go-json v0.9.11 // indirect
	github.com/gomodule/redigo v1.8.4 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/minio/highwayhash v1.0.2 // indirect
	github.com/nats-io/jwt/v2 v2.3.0 // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/nguyenthenguyen/docx v0.0.0-20220721043308-1903da0ef37d // indirect
	github.com/nyaruka/phonenumbers v1.1.2 // indirect
	github.com/pelletier/go-toml/v2 v2.0.6 // indirect
	golang.org/x/time v0.0.0-20220722155302-e5dcc9cfc0b9 // indirect
	gorm.io/driver/mysql v1.3.3 // indirect
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	gorm.io/driver/postgres v1.3.5
)

require (
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.12.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.11.0 // indirect
	github.com/jackc/pgx/v4 v4.16.0 // indirect
	github.com/joho/godotenv v1.4.0
	golang.org/x/crypto v0.3.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)

require (
	github.com/gin-gonic/gin v1.8.1
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	google.golang.org/genproto v0.0.0-20210903162649-d08c68adba83 // indirect
	gorm.io/datatypes v1.0.6
)

require (
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator/v10 v10.11.1 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/swaggo/gin-swagger v1.5.3
	github.com/ugorji/go/codec v1.2.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

require (
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gofrs/uuid v4.3.0+incompatible
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/files v0.0.0-20220728132757-551d4a08d97a
	golang.org/x/tools v0.1.12 // indirect
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/smartystreets/goconvey v1.7.2
	github.com/swaggo/swag v1.8.1
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/googollee/go-socket.io v1.6.1
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/lib/pq v1.10.6 // indirect
	github.com/nats-io/nats.go v1.20.0
	github.com/smartystreets/assertions v1.2.0 // indirect
)
