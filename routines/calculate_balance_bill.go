package routines

import (
	"gitlab.com/felkis60/cashflow-service/database"
	"gitlab.com/felkis60/cashflow-service/helpers"
	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"
)

func CalculateBalanceForBills() error {

	var bills []types.Bill
	if result := database.Db.Find(&bills); result.Error != nil {
		return result.Error
	}

	var billRep = repository.BillRepository{Db: database.Db}

	for i := range bills {
		database.RedisDB.Del(database.RedisContext, helpers.GenerateBillBalanceCacheKey(bills[i].ID))
		if err := billRep.AddBalance(&bills[i]); err != nil {
			return err
		}
	}

	return nil
}
