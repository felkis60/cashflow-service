package routines

import (
	"log"

	db "gitlab.com/felkis60/cashflow-service/database"
	t "gitlab.com/felkis60/cashflow-service/types"
)

func SyncAll() {
	var account t.Account
	if result := db.Db.First(&account, "name = 'REX'"); result.RowsAffected == 0 {
		log.Println("ERROR: SYNC: No rex account!")
		return
	}
	if err := syncCategories(account.ID); err != nil {
		log.Print(err)
	}
	if err := syncOperations(account.ID, account.Token); err != nil {
		log.Print(err)
	}

}
