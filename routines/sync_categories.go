package routines

import (
	"log"

	"gitlab.com/felkis60/cashflow-service/database"
	pro "gitlab.com/felkis60/cashflow-service/providers"
	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"
)

func syncCategories(accountID int64) error {

	rexCategories, err := pro.RexProvider.CategoriesList()
	if err != nil {
		log.Print(err)
		return err
	}

	var tempCatRep = repository.CategoryRepository{Db: database.Db}

	var queueCategory = make(map[int64][]types.RexCategory)

	for i := range *rexCategories {
		loopCreateCategory(&tempCatRep, &queueCategory, &(*rexCategories)[i], accountID)
	}
	return nil
}

func loopCreateCategory(tempCatRep *repository.CategoryRepository, queueCategory *map[int64][]types.RexCategory, rexCategory *types.RexCategory, accountID int64) {
	var existingOperation types.Operation
	if result := database.Db.Find(&existingOperation, "ext_id = ?", rexCategory.ID); result.RowsAffected > 0 {
		return
	}

	var tempInput types.InputCreateCategory

	if rexCategory.ParentID > 0 {
		var existingParentCategory types.Category
		if result := database.Db.Find(&existingParentCategory, "ext_id = ?", rexCategory.ParentID); result.RowsAffected > 0 {
			tempInput.ParentUID = &existingParentCategory.UID
		} else {
			if tempMapKey, ok := (*queueCategory)[rexCategory.ParentID]; ok {
				tempMapKey = append(tempMapKey, *rexCategory)
				(*queueCategory)[rexCategory.ParentID] = tempMapKey
			} else {
				(*queueCategory)[rexCategory.ParentID] = []types.RexCategory{*rexCategory}
			}
			return
		}
	}

	tempInput.Name = rexCategory.Name
	tempInput.Description = rexCategory.Description
	tempInput.ExtID = rexCategory.ID
	tempInput.OperationType = rexCategory.OperationType

	createdCategory, err := tempCatRep.AdminCreate(&tempInput, accountID, tempInput.ExtID)
	if err != nil {
		log.Print(err)
	}

	if tempMapKey, ok := (*queueCategory)[createdCategory.ID]; ok {
		for i := range tempMapKey {
			loopCreateCategory(tempCatRep, queueCategory, &(tempMapKey[i]), accountID)
		}
	}
}
