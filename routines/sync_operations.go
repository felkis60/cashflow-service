package routines

import (
	"log"
	"strconv"
	"time"

	"gitlab.com/felkis60/cashflow-service/database"
	pro "gitlab.com/felkis60/cashflow-service/providers"
	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"
)

func loadUsersIDs() *map[int64]int64 {
	var authUsers []map[string]interface{}
	if database.AuthDB != nil {
		if result := database.AuthDB.Table("users").Find(&authUsers); result.Error != nil {
			log.Print(result.Error)
		}
	}
	var extIDToIDMap = make(map[int64]int64)
	for i := range authUsers {
		if authUsers[i]["id"] != nil && authUsers[i]["ext_id"] != nil {
			extIDToIDMap[authUsers[i]["ext_id"].(int64)] = authUsers[i]["id"].(int64)
		} else {
			log.Print("ERROR: no id or ext_id in authUsers")
		}
	}
	return &extIDToIDMap
}

func syncOperations(accountID int64, accountToken string) error {

	rexOperations, err := pro.RexProvider.OperationList()
	if err != nil {
		log.Print(err)
		return err
	}

	extIDToIDMap := loadUsersIDs()

	var tempOperationRep = repository.OperationRepository{Db: database.Db}

	for i := range *rexOperations {
		//create_users_id
		var tempCount int64
		log.Print((*rexOperations)[i].ID)
		database.Db.Unscoped().Table("operations").Where("operations.ext_id = ?", (*rexOperations)[i].ID).Count(&tempCount)
		if tempCount > 0 {
			log.Print("Operation already exist")
			continue
		}

		var tempOrderUID, tempUserUID, tempVehicleUID string
		var ok bool

		var existingUser map[string]interface{}
		var existingVehicle map[string]interface{}
		var existingRansomOrder map[string]interface{}

		if (*rexOperations)[i].RentUsersID > 0 {
			var rentUserId int64
			if (*rexOperations)[i].RentUsersID == 97 {
				rentUserId = 53
			} else if (*rexOperations)[i].RentUsersID == 91 {
				rentUserId = 79
			} else if (*rexOperations)[i].RentUsersID == 96 {
				rentUserId = 10
			} else if (*rexOperations)[i].RentUsersID == 96 {
				rentUserId = 10
			} else {
				rentUserId = (*rexOperations)[i].RentUsersID
			}

			if result := database.VehileDB.Table("users").Find(&existingUser, "ext_id = ?", rentUserId); result.RowsAffected == 0 {
				log.Print("No such user", rentUserId)
				continue
			}

			tempUserUID, ok = existingUser["uid"].(string)
			if !ok {
				log.Print("No UID in user")
				continue
			}
		}

		if (*rexOperations)[i].VehiclesID > 0 {
			if result := database.VehileDB.Table("vehicles").Find(&existingVehicle, "(ext_id = ? OR ? = ANY(other_ext_ids))", (*rexOperations)[i].VehiclesID, (*rexOperations)[i].VehiclesID); result.RowsAffected == 0 {
				log.Print("No such vehicle")
				continue
			}

			tempVehicleUID, ok = existingVehicle["uid"].(string)
			if !ok {
				log.Print("No UID in vehicle")
				continue
			}

			if (*rexOperations)[i].RentUsersID > 0 {
				log.Print(existingVehicle["id"], existingUser["id"], " "+(*rexOperations)[i].Date)
				if result := database.VehileDB.Table("ransom_orders").Find(&existingRansomOrder, "vehicles_id = ? AND users_id = ? AND ((to_char(ransom_orders.created_at, 'YYYY-MM-DD') <= ? OR ransom_orders.contract_date <= ?) AND (contract_date_end IS NULL OR contract_date_end >= ?))", existingVehicle["id"], existingUser["id"], (*rexOperations)[i].Date, (*rexOperations)[i].Date, (*rexOperations)[i].Date); result.RowsAffected == 0 {
					log.Print("No such ransom_order with vehicles_id = ", existingVehicle["id"])
					continue
				}
				tempOrderUID, ok = existingRansomOrder["uid"].(string)
				if !ok {
					log.Print("No UID in ransom_order")
					continue
				}
			}
		}
		var tempAuthUserId = int64(0)
		if (*rexOperations)[i].UsersID > 0 {
			tempAuthUserId, _ = (*extIDToIDMap)[(*rexOperations)[i].UsersID]
		}

		//Make input
		var tempInput types.InputCreateOperation

		if tempUserUID == "" && tempVehicleUID != "" {
			tempInput.ToUID = tempVehicleUID
			tempInput.ToSource = "vehicle"
		} else if tempUserUID != "" && tempVehicleUID == "" {
			tempInput.ToUID = tempUserUID
			tempInput.ToSource = "user"
		} else if tempUserUID != "" && tempVehicleUID != "" && tempOrderUID != "" {
			tempInput.ToUID = tempOrderUID
			tempInput.ToSource = "ransom_order"
			tempInput.RelationsUIDs = &[]string{tempUserUID, tempVehicleUID}
			tempInput.RelationsSources = &[]string{"user", "vehicle"}
		} else {
			log.Print("Operation without UIDs ID: " + strconv.FormatInt((*rexOperations)[i].ID, 10))
			continue
		}

		tempInput.Amount, err = strconv.ParseFloat((*rexOperations)[i].Amount, 64)
		if err != nil {
			log.Print(err)
		}
		(*rexOperations)[i].Date += " +0300"
		tempDate, err := time.Parse("2006-01-02 15:04:05 -0700", (*rexOperations)[i].Date)
		if err != nil {
			log.Print(err)
		} else {
			tempInput.Date = &tempDate
		}

		tempInput.Description = &(*rexOperations)[i].Description
		tempInput.Deposit = &(*rexOperations)[i].Deposit
		tempInput.ExtID = &(*rexOperations)[i].ID
		tempInput.Status = (*rexOperations)[i].Status
		tempInput.CategoriesID = &(*rexOperations)[i].CategoryID
		switch (*rexOperations)[i].CashType {
		case "cash":
			tempInput.CashType = "CASH"
		default:
			tempInput.CashType = "SETTLEMENT"
		}

		if opera, err := tempOperationRep.Create(&tempInput, accountID, tempAuthUserId); err != nil {
			log.Print(err)
		} else {
			if (*rexOperations)[i].AchiveAt != "" {
				(*rexOperations)[i].AchiveAt += " +0300"
				if tempAchiveDate, err := time.Parse("2006-01-02 15:04:05 -0700", (*rexOperations)[i].AchiveAt); err != nil {
					log.Print(err)
				} else {
					if !tempAchiveDate.IsZero() {
						tempOperationRep.Delete(opera.UID, accountToken, 0)
					}
				}
			}
		}

	}
	return nil
}
