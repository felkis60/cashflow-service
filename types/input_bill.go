package types

type InputCreateEditBill struct {
	MainRelationUID *string `json:"main_relation_uid"`
	ExtID           *int64  `json:"ext_id"`
	Code            *string `json:"code"`
	Name            *string `json:"name"`
	CashType        *string `json:"cash_type" example:"CASH|SETTLEMENT"`
}
