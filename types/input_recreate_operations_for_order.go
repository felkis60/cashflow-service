package types

type InputRecreateOperationsForOrder struct {
	OrderUID    string  `json:"order_uid"`
	UserUID     string  `json:"user_uid"`
	VehicleUID  string  `json:"vehicle_uid"`
	DateStart   string  `json:"date_start"`
	DateEnd     *string `json:"date_end"`
	Description string  `json:"description"`
	Amount      float64 `json:"amount"`
	OrderType   string  `json:"order_type"`
}
