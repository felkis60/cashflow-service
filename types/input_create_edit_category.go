package types

type InputCreateCategory struct {
	ExtID         int64   `json:"ext_id"`
	ParentUID     *string `json:"parent_id"`
	OperationType string  `json:"operation_type" example:"CREDIT|DEBIT"`
	Name          string  `json:"name"`
	Description   string  `json:"description"`
}

type InputEditCategory struct {
	ExtID         *int64  `json:"ext_id"`
	ParentUID     *string `json:"parent_id"`
	OperationType *string `json:"operation_type" example:"CREDIT|DEBIT"`
	Name          *string `json:"name"`
	Description   *string `json:"description"`
}
