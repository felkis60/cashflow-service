package types

import (
	"time"

	"gorm.io/datatypes"
	"gorm.io/gorm"
)

func CheckOperationCashType(cashType string) bool {
	switch cashType {
	case "CASH":
	case "SETTLEMENT":
	default:
		return false
	}
	return true
}

func CheckOperationStatus(status string) bool {
	switch status {
	case "DEFAULT":
	case "SUCCESS":
	case "FAILED":
	default:
		return false
	}

	return true
}

func CheckOperationSource(source string) bool {
	switch source {
	case "vehicle":
	case "user":
	case "ransom_order":
	case "rent_order":
	case "bill":
	default:
		return false
	}
	return true
}

type Operation struct {
	ID               int64          `json:"id" gorm:"primarykey"`
	UID              string         `json:"uid" gorm:"not null,index"`
	ExtID            int64          `json:"ext_id"`
	AccountsID       int64          `json:"accounts_id" gorm:"not null"`
	CategoriesID     int64          `json:"categories_id"`
	RelationsUIDs    datatypes.JSON `json:"relations_uids" swaggertype:"string"`
	RelationsSources datatypes.JSON `json:"relations_sources" swaggertype:"string"`
	FromUID          string         `json:"from_uid"`
	FromSource       string         `json:"from_source"`
	ToUID            string         `json:"to_uid"`
	ToSource         string         `json:"to_source"`
	Amount           float64        `json:"amount" gorm:"not null"`
	Description      string         `json:"description"`
	Date             time.Time      `json:"date" gorm:"not null"`
	Deposit          bool           `json:"deposit" gorm:"not null"`
	CashType         string         `json:"cash_type"`
	Status           string         `json:"status"`
	CreateUsersID    int64          `json:"create_users_id"`
	ToBillsID        *int64         `json:"to_bills_id"`
	FromBillsID      *int64         `json:"from_bills_id"`
	CreatedAt        time.Time      `json:"created_at"`
	UpdatedAt        time.Time      `json:"updated_at"`
	DeletedAt        gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

type OperationWithSaldo struct {
	Operation Operation `gorm:"embedded"`
	Saldo     float64   `json:"saldo"`
}

type MiniOperation struct {
	ToUID    string  `json:"to_uid"`
	Coalesce float64 `json:"coalesce"`
}
