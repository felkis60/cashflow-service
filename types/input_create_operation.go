package types

import (
	"time"
)

type InputCreateOperation struct {
	ExtID            *int64     `json:"ext_id"`
	CategoriesID     *int64     `json:"categories_id"`
	RelationsUIDs    *[]string  `json:"relations_uids"`
	RelationsSources *[]string  `json:"relations_sources" example:"vehicle|user|ransom_order|rent_order|bill"`
	FromUID          *string    `json:"from_uid"`
	FromSource       *string    `json:"from_source" example:"vehicle|user|ransom_order|rent_order|bill"`
	ToUID            string     `json:"to_uid"`
	ToSource         string     `json:"to_source" example:"vehicle|user|ransom_order|rent_order|bill"`
	Amount           float64    `json:"amount"`
	Description      *string    `json:"description"`
	Date             *time.Time `json:"date"`
	Deposit          *bool      `json:"deposit"`
	CashType         string     `json:"cash_type" example:"CASH|SETTLEMENT"`
	Status           string     `json:"status" example:"FAILED|SUCCESS|DEFAULT"`
	ToBillsID        *int64     `json:"to_bills_id"`
	FromBillsID      *int64     `json:"from_bills_id"`
}

type InputEditOperation struct {
	ExtID            *int64     `json:"ext_id"`
	CategoriesID     *int64     `json:"categories_id"`
	RelationsUIDs    *[]string  `json:"relations_uids"`
	RelationsSources *[]string  `json:"relations_sources" example:"vehicle|user|ransom_order|rent_order|bill"`
	FromUID          *string    `json:"from_uid"`
	FromSource       *string    `json:"from_source" example:"vehicle|user|ransom_order|rent_order|bill"`
	ToUID            *string    `json:"to_uid"`
	ToSource         *string    `json:"to_source" example:"vehicle|user|ransom_order|rent_order|bill"`
	Amount           *float64   `json:"amount"`
	Description      *string    `json:"description"`
	Date             *time.Time `json:"date"`
	Deposit          *bool      `json:"deposit"`
	CashType         *string    `json:"cash_type" example:"CASH|SETTLEMENT"`
	Status           *string    `json:"status" example:"FAILED|SUCCESS|DEFAULT"`
	ToBillsID        *int64     `json:"to_bills_id"`
	FromBillsID      *int64     `json:"from_bills_id"`
}

type InputDeleteOperationsForOrder struct {
	AccountToken string `json:"-"`
	AccountID    int64  `json:"-"`
	AuthUsersID  int64  `json:"-"`
	UID          string `json:"uid"`
	RelationsUID string `json:"relations_uid"`
	ToSource     string `json:"to_source"`
	CategoriesID int64  `json:"categories_id"`
}
