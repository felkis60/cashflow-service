package types

import (
	"time"

	"gorm.io/gorm"
)

type Account struct {
	ID        int64          `json:"id" gorm:"primarykey"`
	Name      string         `json:"name"`
	Token     string         `json:"token" gorm:"unique, not null"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
