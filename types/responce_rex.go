package types

type RexPaginationOperation struct {
	Items      []RexOperation `json:"items"`
	TotalPages uint64         `json:"total_pages"`
	TotalItems uint64         `json:"total_items"`
}

type RexPaginationCategories struct {
	Items      []RexCategory `json:"items"`
	TotalPages uint64        `json:"total_pages"`
	TotalItems uint64        `json:"total_items"`
}

type ResponceRexListOperations struct {
	Payload RexPaginationOperation `json:"payload"`
	Message string                 `json:"message"`
	Code    string                 `json:"code"`
	Status  int                    `json:"status"`
}

type ResponceRexListCategories struct {
	Payload RexPaginationCategories `json:"payload"`
	Message string                  `json:"message"`
	Code    string                  `json:"code"`
	Status  int                     `json:"status"`
}

type ResponceRex struct {
	Payload interface{}
	Message string
	Code    string
	Status  int
}

type RexOperation struct {
	ID              int64  `json:"id"`
	CategoryID      int64  `json:"category_id"`
	UsersID         int64  `json:"users_id"`
	UID             string `json:"uid"`
	Description     string `json:"description"`
	Amount          string `json:"amount"`
	Date            string `json:"date"`
	CashflowBoxesID int64  `json:"cashflow_boxes_id"`
	CashType        string `json:"cash_type"`
	RentUsersID     int64  `json:"rent_users_id"`
	Deposit         bool   `json:"deposit"`
	VehiclesID      int64  `json:"vehicles_id"`
	FromUsersID     int64  `json:"from_users_id"`
	ToUsersID       int64  `json:"to_users_id"`
	Status          string `json:"status"`
	AchiveAt        string `json:"archive_at"`
}

type RexCategory struct {
	ID            int64  `json:"id"`
	UsersID       int64  `json:"users_id"`
	UID           string `json:"uid"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	ParentID      int64  `json:"parent_id"`
	OperationType string `json:"operation_type"`
}
