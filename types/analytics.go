package types

type AnalyticsBalance struct {
	Credit float64 `json:"credit"`
	Debit  float64 `json:"debit"`
}

type SubAnalytics struct {
	FullBalance       float64 `json:"full_balance"`
	FullCredit        float64 `json:"full_credit"`
	FullDebit         float64 `json:"full_debit"`
	CashBalance       float64 `json:"cash_balance"`
	CashCredit        float64 `json:"cash_credit"`
	CashDebit         float64 `json:"cash_debit"`
	SettlementBalance float64 `json:"settlement_balance"`
	SettlementCredit  float64 `json:"settlement_credit"`
	SettlementDebit   float64 `json:"settlement_debit"`
	DayPaymentsCredit float64 `json:"day_payments_credit"`
	FineCredit        float64 `json:"fine_credit"`
	OtherCredit       float64 `json:"other_credit"`
}

type Analytics struct {
	FullBalance float64 `json:"full_balance"`
	FullCredit  float64 `json:"full_credit"`
	FullDebit   float64 `json:"full_debit"`

	CashBalance float64 `json:"cash_balance"`
	CashCredit  float64 `json:"cash_credit"`
	CashDebit   float64 `json:"cash_debit"`

	SettlementBalance float64 `json:"settlement_balance"`
	SettlementCredit  float64 `json:"settlement_credit"`
	SettlementDebit   float64 `json:"settlement_debit"`

	DayPaymentsCredit float64 `json:"day_payments_credit"`
	FineCredit        float64 `json:"fine_credit"`
	OtherCredit       float64 `json:"other_credit"`

	NumberOfOperations int64 `json:"number_of_operations"`

	BalanceByDays   map[string]AnalyticsBalance `json:"balance_by_days"`   //YYYY-MM-DD
	BalanceByMonths map[string]AnalyticsBalance `json:"balance_by_months"` //YYYY-MM

}

type VehicleAnalytics struct {
	BuyDate           string  `json:"buy_date"`            //- находим операцию с категорией ext_id = 14 и берем date операции ////
	VehicleAmount     float64 `json:"vehicle_amount"`      // - находим операции с категорией ext_id = 14 и берем amount операции ////
	Deposit           float64 `json:"deposit"`             //- все операции по машине в deposit = true ////
	DaysCount         uint64  `json:"days_count"`          //- количество дней по операциям с категорией ext_id = 8
	Result            float64 `json:"result"`              //- (сумма всех операций в категориях ext_id IN (10,48,57) AND parent_id{ext_id} IN (49,54,58)) минус (сумма всех операций в категориях ext_id IN (14,16,17,20,21) AND parent_id{ext_id} IN (15,19,23) ////
	MetaVehicleAmount float64 `json:"meta_vehicle_amount"` //- current.vehicleAmount + current.repairCreditAmount + current.otherCreditAmount

	TrackerInstallAmount           float64 `json:"tracker_install_amount"`            //[16]
	TrackerMonthAmount             float64 `json:"tracker_month_amount"`              //[17]
	RegistrationAmount             float64 `json:"registration_amount"`               // [20],
	RepairCreditAmount             float64 `json:"repair_credit_amount"`              // [21],
	OtherCreditAmount              float64 `json:"other_credit_amount"`               // [23],
	VehicleTaxAmount               float64 `json:"vehicle_tax_amount"`                // [19],
	TotalCreditAmount              float64 `json:"total_credit_amount"`               // [14, 16, 17, 20, 21], // = 14 ~ 2000, 17 ~ 1000 = 3000
	RentDebitAmount                float64 `json:"rent_debit_amount"`                 // [10],
	InitialPaymentAmount           float64 `json:"initial_payment_amount"`            // [48],
	CategoryMandatoryPaymentAmount float64 `json:"category_mandatory_payment_amount"` // [49],
	RepairDebitAmount              float64 `json:"repair_debit_amount"`               // [57],
	TotalDebitAmount               float64 `json:"total_debit_amount"`                // [10, 48, 57],
	DayPaymentsAmount              float64 `json:"day_payments_amount"`               // [8],

	//CATEGORIES_PARENTS_IDS
	ParentsOsagoAndDkAmount       float64 `json:"parents_osago_and_dk_amount"`      // [15],
	ParentsTotalCreditAmount      float64 `json:"parents_total_credit_amount"`      // [15, 19, 23], // = 15 ~ 200, 23 ~ 3000 = 3200
	ParentsMandatoryPaymentAmount float64 `json:"parents_mandatory_payment_amount"` // [49, 54],
	ParentsOtherDebitAmount       float64 `json:"parents_other_debit_amount"`       // [58],
	ParentsTotalDebitAmount       float64 `json:"parents_total_debit_amount"`       // [49, 54, 58],

	// totalCreditAmount: 6200

}

type FullAnalytics struct {
	Months map[string]OneMonthFullAnalytics `json:"months"`
}

type OneMonthFullAnalytics struct {
	Debit  map[string]float64 `json:"debit"`
	Credit map[string]float64 `json:"credit"`
}

type BalanaceAndMonth struct {
	Balance float64 `json:"balance"`
	Month   string  `json:"month"`
}
