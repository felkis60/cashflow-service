package types

import (
	"time"

	"gorm.io/datatypes"
)

type History struct {
	ID            int64          `json:"id" gorm:"primarykey"`
	AccountsID    int64          `json:"accounts_id"`
	SourceTable   string         `json:"source_table" gorm:"index"`
	SourceID      int64          `json:"source_id" gorm:"index"`
	OperationType string         `json:"operation_type"`
	After         datatypes.JSON `json:"after"`
	Before        datatypes.JSON `json:"before"`
	UserID        int64          `json:"user_id"`
	UserData      interface{}    `json:"user_data" gorm:"-"`
	CreatedAt     time.Time      `json:"created_at"`
}

type InputGetHistoryList struct {
	Page          int    `json:"page"`
	PageSize      int    `json:"page_size"`
	OrderBy       string `json:"order_by"`
	OrderDir      string `json:"order_direction" example:"asc|desc"`
	Search        string `json:"search"`
	OperationType string `json:"operation_type" example:"create|edit|delete"`
}
