package types

type InputGinHeader struct {
	Authorization string `header:"Authorization"`
}

type InputAccountTokenHeader struct {
	Token string `header:"Token"`
}
