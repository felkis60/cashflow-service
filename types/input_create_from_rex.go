package types

type InputCreateFromRex struct {
	ID          int64  `json:"id"`
	CategoryID  int64  `json:"category_id"`
	UserID      int64  `json:"users_id"`
	UID         string `json:"uid"`
	Description string `json:"description"`
	Amount      string `json:"amount"`
	Date        string `json:"date"`
	RentUserID  int64  `json:"rent_users_id"`
	Deposit     bool   `json:"deposit"`
	CashType    string `json:"cash_type"`
	VehicleID   int64  `json:"vehicles_id"`
	Status      string `json:"status"`
	AchiveAt    string `json:"archive_at"`
}
