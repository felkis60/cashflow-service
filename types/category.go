package types

import (
	"time"

	"gorm.io/gorm"
)

func CheckOperationType(operationType string) bool {
	switch operationType {
	case "CREDIT":
	case "DEBIT":
	default:
		return false
	}
	return true
}

type Category struct {
	ID            int64          `json:"id" gorm:"primarykey"`
	UID           string         `json:"uid" gorm:"not null"`
	ExtID         int64          `json:"ext_id"`
	AccountsID    int64          `json:"accounts_id" gorm:"not null"`
	ParentID      int64          `json:"parent_id"`
	OperationType string         `json:"operation_type" gorm:"not null"`
	Name          string         `json:"name" gorm:"not null"`
	Description   string         `json:"description"`
	CreatedAt     time.Time      `json:"created_at"`
	UpdatedAt     time.Time      `json:"updated_at"`
	DeletedAt     gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}
