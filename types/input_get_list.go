package types

type CategoryListFilters struct {
	OperationType *string `json:"operation_type"`
}

type OperationListFilters struct {
	CashType             *string   `json:"cash_type" example:"SETTLEMENT|CASH"`
	BillCashType         *string   `json:"bill_cash_type" example:"SETTLEMENT|CASH"`
	AmountType           *string   `json:"amount_type" example:"credit|debit"`
	RelationsUIDs        *[]string `json:"relations_uids"`
	OrRelationsUIDs      *[]string `json:"or_relations_uids"`
	DateStart            *string   `json:"date_start" example:"YYYY-MM-DD"`
	DateEnd              *string   `json:"date_end" example:"YYYY-MM-DD"`
	CreatedAtStart       *string   `json:"created_at_start" example:"YYYY-MM-DD"`
	CreatedAtEnd         *string   `json:"created_at_end" example:"YYYY-MM-DD"`
	CategoriesID         *int64    `json:"categories_id"`
	InCategoriesID       *[]int64  `json:"in_categories_id"`
	ToUID                *string   `json:"to_uid"`
	InToUIDs             *[]string `json:"in_to_uids"`
	Deposit              *bool     `json:"deposit"`
	WithoutCache         *bool     `json:"without_cache"`
	SearchUID            *string   `json:"search_uid"`
	ToUIDAndRelations    *string   `json:"to_uid_and_relations"`
	Search               *string   `json:"search"`
	WithoutCategoriesIDs *[]int64  `json:"without_categories_ids"`
	ToBillsID            *int64    `json:"to_bills_id"`
	ToSource             *string   `json:"to_source"`
	MisleadingDate       *bool     `json:"misleading_date"`
	GroupBy              *string   `json:"group_by"`
}

type InputBillBalanceByDays struct {
	DateStart *string `json:"date_start" example:"YYYY-MM-DD"`
	DateEnd   *string `json:"date_end" example:"YYYY-MM-DD"`
}

type InputGetListOperations struct {
	Page     int                  `json:"page"`
	PageSize int                  `json:"page_size"`
	OrderBy  string               `json:"order_by"`
	OrderDir string               `json:"order_direction" example:"asc|desc"`
	Search   string               `json:"search"`
	Filters  OperationListFilters `json:"filters"`
}

type InputGetList struct {
	Page     int    `json:"page"`
	PageSize int    `json:"page_size"`
	OrderBy  string `json:"order_by"`
	OrderDir string `json:"order_direction" example:"asc|desc"`
	Search   string `json:"search"`
}

type InputListBill struct {
	Force    bool   `json:"force"`
	Page     int    `json:"page"`
	PageSize int    `json:"page_size"`
	OrderBy  string `json:"order_by"`
	OrderDir string `json:"order_direction" example:"asc|desc"`
	Search   string `json:"search"`
}

type InputGetListCategory struct {
	Page     int                  `json:"page"`
	PageSize int                  `json:"page_size"`
	OrderBy  string               `json:"order_by"`
	OrderDir string               `json:"order_direction" example:"asc|desc"`
	Search   string               `json:"search"`
	Filters  *CategoryListFilters `json:"filters"`
}
