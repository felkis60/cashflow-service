package types

import (
	"time"

	"gorm.io/gorm"
)

type Bill struct {
	ID         int64          `json:"id" gorm:"primarykey"`
	UID        string         `json:"uid" gorm:"not null"`
	ExtID      int64          `json:"ext_id"`
	AccountsID int64          `json:"accounts_id" gorm:"not null"`
	Code       string         `json:"code" gorm:"not null"`
	Name       string         `json:"name" gorm:"not null"`
	CashType   string         `json:"cash_type" gorm:"default:SETTLEMENT"`
	Balance    float64        `json:"balance" gorm:"-"`
	CreatedAt  time.Time      `json:"created_at"`
	UpdatedAt  time.Time      `json:"updated_at"`
	DeletedAt  gorm.DeletedAt `json:"deleted_at" gorm:"index" swaggertype:"string"`
}

type MiniBill struct {
	Amount            float64 `json:"amount"`
	Deposit           float64 `json:"deposit"`
	DaysOfDelay       uint64  `json:"days_of_delay"`
	DayPaymentsAmount float64 `json:"day_payments_amount"`
	DaysOfDelayAmount float64 `json:"days_of_delay_amount"`
	Residual          float64 `json:"residual"`
	RentDebitAmount   float64 `json:"rent_debit_amount"`
}

type BillByDay struct {
	Amount               float64 `json:"amount"`
	MisleadingOperations int64   `json:"misleading_operations"`
}
