package startup

import (
	"log"
	"path"
	"runtime"

	db "gitlab.com/felkis60/cashflow-service/database"
	l "gitlab.com/felkis60/cashflow-service/log"

	"github.com/joho/godotenv"
)

var (
	basicInited = false
	envInited   = false
	logsInited  = false
	redis       = false
	natsInited  = false
)

func SystemStartup(notTestEnv_ bool, logs_ bool, additionalDBs_ bool) {

	if !envInited {
		if notTestEnv_ {
			if err := godotenv.Load("configs/.env"); err != nil {
				log.Fatal(err)
			}
		} else {
			_, filename, _, _ := runtime.Caller(0)
			dir := path.Join(path.Dir(filename), "..")
			if err := godotenv.Load(dir + "/configs/t.env"); err != nil {
				log.Fatal(err)
			}
		}
		envInited = true
	}

	if logs_ {
		if !logsInited {
			logsInited = true
			l.StartLogs()
		}
	}

	if !basicInited {
		basicInited = true
		db.Start()
		db.Migrate()
	}

	// TODO disable when rex 2012222 is deprecated
	if additionalDBs_ {
		db.StartVehicles()
		db.StartAuth()
	}
	// ---------

	if !redis {
		redis = true
		db.InitRedis()
	}

}
