## Запуск 
    go run .
    go run ./api/rest/server/.
    go run ./api/grpc/server/.

## Тесты
	go test ./api/rest/test/.

## Swagger
    swag init -o ./api/rest/server/docs

## GRPC Init
    protoc --go_out=. --go_opt=paths=source_relative api/grpc/app/cashflow.proto
    protoc --go-grpc_out=. --go-grpc_opt=paths=source_relative api/grpc/app/cashflow.proto