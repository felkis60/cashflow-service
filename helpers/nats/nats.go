package nats

import (
	"log"
	"os"
	"time"

	"github.com/nats-io/nats.go"
)

var MainConnection *nats.Conn
var EncodedConnection *nats.EncodedConn

func Init() error {

	var err error

	MainConnection, err = nats.Connect("nats://127.0.0.1:"+os.Getenv("NATSPORT"),
		nats.RetryOnFailedConnect(true),
		nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
			log.Printf("ERROR: NATS: Got disconnected! Reason: %q\n", err)
		}),
		nats.ReconnectHandler(func(nc *nats.Conn) {
			log.Printf("INFO: NATS: Got reconnected to %v!\n", nc.ConnectedUrl())
		}),
		nats.ClosedHandler(func(nc *nats.Conn) {
			log.Printf("INFO: NATS: Connection closed. Reason: %q\n", nc.LastError())
		}), nats.ReconnectWait(time.Second), nats.MaxReconnects(-1))

	if err != nil {
		return err
	}

	EncodedConnection, err = nats.NewEncodedConn(MainConnection, nats.JSON_ENCODER)
	if err != nil {
		return err
	}

	return nil
}

func WaitMessage() {

	ch := make(chan *nats.Msg, 64)
	_, err := MainConnection.ChanSubscribe("*", ch)
	if err != nil {
		log.Print("ERROR: nats channel failed: " + err.Error())
		return
	}

	for {
		msg1 := <-ch
		log.Println("INFO: ", msg1)
	}

	//sub.Unsubscribe()
}
