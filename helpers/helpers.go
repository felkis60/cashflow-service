package helpers

import (
	"errors"
	"sort"
	"strconv"
	"strings"
	"time"

	types "gitlab.com/felkis60/cashflow-service/types"
)

func TimeIntersection(checkStart, checkEnd, existingStart, existingEnd time.Time) bool {
	if checkStart.IsZero() {
		return !checkEnd.Before(existingStart)
	}

	if checkEnd.IsZero() {
		return !checkStart.After(existingEnd)
	}

	return (!checkStart.After(existingEnd) && !checkEnd.Before(existingStart))
}

func MakeRedisKeySummAmount(accountID int64, input *types.OperationListFilters) (string, error) {
	resultString := strconv.FormatInt(accountID, 10)
	if input.CategoriesID != nil {
		resultString += "_(1)" + strconv.FormatInt(*input.CategoriesID, 10)
	}
	if input.ToUID != nil {
		resultString += "_(2)" + *input.ToUID
	}
	if input.DateStart != nil && *input.DateStart != "" {
		resultString += "_(3)" + *input.DateStart
	}
	if input.DateEnd != nil && *input.DateEnd != "" {
		resultString += "_(4)" + *input.DateEnd
	}
	if input.RelationsUIDs != nil {
		tempSorted, err := SortAndUniteUIDs(input.RelationsUIDs)
		if err != nil {
			return "", err
		}
		resultString += "_(5)" + *tempSorted

	}
	if input.SearchUID != nil {
		resultString += "_(6)" + *input.SearchUID
	}
	if input.SearchUID != nil {
		resultString += "_(7)" + *input.SearchUID
	}
	if input.Deposit != nil {
		resultString += "_(8)deposit=" + strconv.FormatBool(*input.Deposit)
	}
	if input.CashType != nil {
		resultString += "_(9)" + *input.CashType
	}
	if input.AmountType != nil {
		resultString += "_(10)" + *input.AmountType
	}
	if input.ToUIDAndRelations != nil {
		resultString += "_(11)" + *input.ToUIDAndRelations
	}
	if input.Search != nil {
		resultString += "_(12)" + *input.Search
	}

	AddServiceNameToRedisKey(&resultString)

	return resultString, nil
}

func SortUIDs(input *[]string) (*[]string, error) {
	sort.Strings(*input)
	return input, nil
}

func SortAndUniteUIDs(input *[]string) (*string, error) {
	arr, err := SortUIDs(input)
	if err != nil {
		return nil, err
	}

	var tempString = strings.Join(*arr, "_")

	return &tempString, nil
}

func AddServiceNameToRedisKey(key *string) {
	*key = "[cashflow-service]" + *key
}

func AddServiceNameToError(err error) error {
	return errors.New("[cashflow-service]" + err.Error())
}

func GenerateBillBalanceCacheKey(billID int64) string {

	var cacheKey string
	AddServiceNameToRedisKey(&cacheKey)
	cacheKey += "bill_balance_" + strconv.FormatInt(billID, 10)

	return cacheKey

}
