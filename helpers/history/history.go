package history

import (
	"encoding/json"

	db "gitlab.com/felkis60/cashflow-service/database"
	"gitlab.com/felkis60/cashflow-service/types"
)

func CreateHistory(id int64, table string, operationType string, before interface{}, after interface{}, authUserID int64, accountID int64) error {

	JSONBefore, err := json.Marshal(before)
	if err != nil {
		return err
	}
	JSONAfter, err := json.Marshal(after)
	if err != nil {
		return err
	}

	var data types.History
	data.After = JSONAfter
	data.Before = JSONBefore
	data.SourceID = id
	data.SourceTable = table
	data.OperationType = operationType
	data.UserID = authUserID
	data.AccountsID = accountID

	db.Db.Create(&data)

	return nil
}
