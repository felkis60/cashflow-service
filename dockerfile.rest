FROM golang:1.18.9-alpine3.17 AS build-env
ENV GOPATH /go
WORKDIR /go/src/cashflow_service
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . /go/src/cashflow_service
RUN go build -o ./rest-server ./api/rest/server/server.go

FROM alpine
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk*
WORKDIR /app
COPY --from=build-env /go/src/cashflow_service /app
COPY ./configs/.env /app

EXPOSE 8080

ENTRYPOINT [ "./rest-server" ]