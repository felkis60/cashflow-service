package log

import (
	"log"
	"os"
)

func StartLogs() {

	if os.Getenv("log_file") == "true" {
		file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		log.SetOutput(file)
	}

	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if os.Getenv("INFOLOG") == "true" {
		PrintInfo = true
	}
}

const (
	ERR                           = "ERROR: %v"
	ERRWrongData                  = "ERROR: Wrong data"
	ERREnvLoad                    = "ERROR: Failed to load .env file"
	ERRDbConnect                  = "ERROR: Failed to connect database: %v"
	ERRAutoMigrate                = "ERROR: Failed to automigrate: %v"
	ERRListen                     = "ERROR: Failed to listen: %v"
	ERRServe                      = "ERROR: Failed to serve: %v"
	ERRTokenReq                   = "ERROR: Account Token is required"
	ERRUIDReq                     = "ERROR: 'uid' is required"
	ERRPhoneReq                   = "ERROR: 'phone' is required"
	ERROrderUIDReq                = "ERROR: 'order_uid' is required"
	ERRBillNameReq                = "ERROR: 'name' is required"
	ERRBillCodeReq                = "ERROR: 'code' is required"
	ErrAmountReq                  = "ERROR: 'amount' is required"
	ERRToUIDReq                   = "ERROR: 'to_uid' is required"
	ERRToSourceReq                = "ERROR: 'to_source' is required"
	ERRWrongOperationCashType     = "ERROR: Wrong operation 'cash_type'"
	ERRWrongOperationStatus       = "ERROR: Wrong operation 'status'"
	ErrDateReq                    = "ERROR: 'date' is required"
	ERRNoSuchToken                = "ERROR: No such Account Token in Cashflow Service"
	ERRNoSuchOrder                = "ERROR: No such order"
	ERRNoSuchCategory             = "ERROR: No such category"
	ERRNoSuchUID                  = "ERROR: No such 'uid'"
	ERRNoSuchCategoryUID          = "ERROR: No such Category UID"
	ERRNoSuchOperationUID         = "ERROR: No such Operation UID"
	ERRTokenIsOccupied            = "ERROR: Token is occupied"
	ERRSameUID                    = "ERROR: Same UID, need another"
	ERRSameBillID                 = "ERROR: Same Bill ID, need another"
	ERRRedisNotInited             = "ERROR: Redis not inited"
	ERRParentToYourself           = "ERROR: Cannot be parent to yourself"
	ERRWrongCategoryOperationType = "ERROR: Wrong category 'operation_type'"
	ERRNoSuchBillUID              = "ERROR: No such Bill UID"
	ERRNoSuchBillID               = "ERROR: No such Bill ID"
	ERRNoSuchBillMainRelationUID  = "ERROR: No such Bill Main Relation UID"
	ERRBillCodeAlreadyExists      = "ERROR: Bill with the Code already exists"
	ERRNegativeDeposit            = "ERROR: Deposit will be negative"
	ERRRelationsSameElements      = "ERROR: 'relations_sources' and 'relations_uids' must contain same number of elements"

	INF            = "INFO: %v"
	INFReceived    = "INFO: Received: %v"
	INFStartServer = "INFO: %v server starts"
	INFEndServer   = "INFO: %v server ends"
	INFFuncStart   = "INFO: Function %v start"
	INFFuncEnd     = "INFO: Function %v end"
)

var (
	PrintInfo = false
)
