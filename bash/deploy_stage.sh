#!/bin/bash

docker build -f dockerfile.rest -t cashflow-service-rest_stage . --network=host
docker build -f dockerfile.grpc -t cashflow-service-grpc_stage . --network=host
docker stop cashflow-service-rest_stage || true
docker stop cashflow-service-grpc_stage || true
docker rm cashflow-service-rest_stage || true
docker rm cashflow-service-grpc_stage || true
docker run --network host -d -p 6316:6316 -v `pwd`:/srv/app --name cashflow-service-rest_stage cashflow-service-rest_stage
docker run --network host -d -p 6317:6317 -v `pwd`:/srv/app --name cashflow-service-grpc_stage cashflow-service-grpc_stage