#!/bin/bash

docker build -f dockerfile.rest -t cashflow-service-rest . --network=host
docker build -f dockerfile.grpc -t cashflow-service-grpc . --network=host
docker stop cashflow-service-rest || true
docker stop cashflow-service-grpc || true
docker rm cashflow-service-rest || true
docker rm cashflow-service-grpc || true
docker run --network host -d -p 6306:6306 -v `pwd`:/srv/app --name cashflow-service-rest cashflow-service-rest
docker run --network host -d -p 6307:6307 -v `pwd`:/srv/app --name cashflow-service-grpc cashflow-service-grpc