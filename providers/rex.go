package providers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	t "gitlab.com/felkis60/cashflow-service/types"
)

type RexProviderType string

const RexProvider RexProviderType = "https://rent.2012222.ru/"

func (provider RexProviderType) OperationList() (*[]t.RexOperation, error) {

	var maxPages = 2

	var allOperations []t.RexOperation

	for i := 1; i <= maxPages; i++ {
		req, err := http.NewRequest("GET", string(provider)+"api/v1/cashflow/list?page="+strconv.Itoa(i), nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("X-Auth-Public-Token", os.Getenv("REX_PROVIDER_TOKEN"))

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		var data t.ResponceRexListOperations
		err = json.Unmarshal(body, &data)
		if err != nil {
			return nil, err
		}
		maxPages = int(data.Payload.TotalPages)
		allOperations = append(allOperations, data.Payload.Items...)
	}

	return &allOperations, nil

}

func (provider RexProviderType) CategoriesList() (*[]t.RexCategory, error) {

	req, err := http.NewRequest("GET", string(provider)+"api/v1/cashflow/categories/list", nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var data t.ResponceRexListCategories
	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}

	return &data.Payload.Items, nil

}
