package database

import (
	"context"
	"log"
	"os"

	"github.com/go-redis/redis/v8"
)

var RedisContext = context.Background()
var RedisDB *redis.Client

// Подключение к базе redis
func InitRedis() {
	RedisDB = redis.NewClient(&redis.Options{
		Addr:     "localhost:" + os.Getenv("REDISPORT"),
		Password: "",
		DB:       0,
	})

	if err := RedisDB.Ping(RedisContext).Err(); err != nil {
		log.Fatalf(err.Error())
	}
}
