package database

import (
	"log"
	"os"

	l "gitlab.com/felkis60/cashflow-service/log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var VehileDB *gorm.DB

// Подключение к базе данных vehicles_service
func StartVehicles() {

	dsn := "host=" + os.Getenv("HOST") +
		" user=" + os.Getenv("DBUSER") +
		" password=" + os.Getenv("DBPASSWORD") +
		" dbname=" + os.Getenv("DBVEHICLESNAME") +
		" port=" + os.Getenv("DBPORT") +
		" sslmode=" + os.Getenv("DBSSLMODE") +
		" TimeZone=" + os.Getenv("DBTIMEZONE")

	var err error
	VehileDB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalf(l.ERRDbConnect, err.Error())
	}
}
