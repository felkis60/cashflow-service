package database

import (
	"log"
	"os"
	"time"

	l "gitlab.com/felkis60/cashflow-service/log"
	t "gitlab.com/felkis60/cashflow-service/types"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var Db *gorm.DB

// Подключение к базе данных cashflow_service
func Start() {

	dsn := "host=" + os.Getenv("HOST") +
		" user=" + os.Getenv("DBUSER") +
		" password=" + os.Getenv("DBPASSWORD") +
		" dbname=" + os.Getenv("DBNAME") +
		" port=" + os.Getenv("DBPORT") +
		" sslmode=" + os.Getenv("DBSSLMODE") +
		" TimeZone=" + os.Getenv("DBTIMEZONE")

	newLogger := logger.New(
		log.New(log.Writer(), "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Millisecond * 300,
			LogLevel:                  logger.Error,
			IgnoreRecordNotFoundError: true,
			Colorful:                  false,
		},
	)

	var err error
	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: newLogger})

	if err != nil {
		log.Fatalf(l.ERRDbConnect, err.Error())
	}
}

// Миграция моделей данных в БД
func Migrate() {
	err := Db.AutoMigrate(&t.Account{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.Bill{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.Category{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.Operation{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.History{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

}
