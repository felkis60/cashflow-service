package database

import (
	"log"
	"os"

	l "gitlab.com/felkis60/cashflow-service/log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var AuthDB *gorm.DB

// Подключение к базе данных auth_service
func StartAuth() {

	dsn := "host=" + os.Getenv("HOST") +
		" user=" + os.Getenv("DBUSER") +
		" password=" + os.Getenv("DBPASSWORD") +
		" dbname=" + os.Getenv("DB_AUTH_NAME") +
		" port=" + os.Getenv("DBPORT") +
		" sslmode=" + os.Getenv("DBSSLMODE") +
		" TimeZone=" + os.Getenv("DBTIMEZONE")

	var err error
	AuthDB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalf(l.ERRDbConnect, err.Error())
	}
}
