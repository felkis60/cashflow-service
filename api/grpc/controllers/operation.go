package controllers

import (
	"context"
	"encoding/json"
	"errors"

	pb "gitlab.com/felkis60/cashflow-service/api/grpc/app"
	"gitlab.com/felkis60/cashflow-service/helpers"
	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"
)

type GRPCOperationServer struct {
	Repository repository.OperationRepository
	pb.OperationServiceServer
}

func (s *GRPCOperationServer) Create(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputCreateOperation
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	operation, err := s.Repository.Create(&tempInput, account.ID, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonOperation, err := json.Marshal(operation)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonOperation}, nil
}

func (s *GRPCOperationServer) CreateMultiple(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput []types.InputCreateOperation
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	operation, err := s.Repository.CreateMultiple(&tempInput, account.ID, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonOperation, err := json.Marshal(operation)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonOperation}, nil
}

func (s *GRPCOperationServer) Edit(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputEditOperation
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	operation, err := s.Repository.Edit(req.UID, &tempInput, req.AccountToken, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonOperation, err := json.Marshal(operation)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonOperation}, nil
}

func (s *GRPCOperationServer) Delete(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {
	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	err = s.Repository.Delete(req.UID, req.AccountToken, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil
}

func (s *GRPCOperationServer) DeleteOperations8ForOrder(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {
	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputDeleteOperationsForOrder
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	tempInput.AccountToken = account.Token
	tempInput.AccountID = account.ID
	tempInput.AuthUsersID = req.UserID

	err = s.Repository.DeleteOperations8ForOrder(&tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil
}

func (s *GRPCOperationServer) Get(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	operation, err := s.Repository.GetByUID(req.UID, req.AccountToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonOperation, err := json.Marshal(operation)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonOperation}, nil
}

func (s *GRPCOperationServer) List(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputGetListOperations
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.List(&tempInput, req.AccountToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) BalanceByDaysForEachBill(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputBillBalanceByDays
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.BalanceByDaysForEachBill(&tempInput, req.AccountToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) History(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputGetHistoryList
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.History(req.UID, req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) HasOperations(ctx context.Context, req *pb.InputDoByJSON) (*pb.SimpleInt64, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return nil, helpers.AddServiceNameToError(err)
	}

	var tempInput types.OperationListFilters
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return nil, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.HasOperations(&tempInput, req.AccountToken)
	if err != nil {
		return nil, helpers.AddServiceNameToError(err)
	}

	if data != nil {
		return &pb.SimpleInt64{Value: *data}, nil
	} else {
		return nil, helpers.AddServiceNameToError(errors.New("ERROR: data is nil"))
	}

}

func (s *GRPCOperationServer) Summ(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.OperationListFilters
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	data, err := s.Repository.Summ(account.ID, req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) BalanceByMonths(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.OperationListFilters
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	data, err := s.Repository.BalanceByMonths(account.ID, req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) Analytics(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.OperationListFilters
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	data, err := s.Repository.Analytics(account.ID, req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) FullAnalytics(ctx context.Context, req *pb.WithoutInput) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.FullAnalytics(account.ID, req.AccountToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) VehicleAnalytics(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputGetVehicleAnalytics
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	}
	data, err := s.Repository.VehicleAnalytics(account.ID, req.AccountToken, req.UID, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) AmountAndDaysOfDelay(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	//var tempInput types.
	//if err := json.Unmarshal(req.Body, &tempInput); err != nil {
	//	return &pb.ServiceResponse{Code: 400, Message: err.Error()}, err
	//}
	data := s.Repository.AmountAndDaysOfDelay(req.UID, req.AccountToken)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCOperationServer) LastPayment(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.LastPayment(account.ID, req.AccountToken, req.UID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil
}

func (s *GRPCOperationServer) SummForRansomOrders(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput map[int64][]string
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	data, err := s.Repository.SummForRansomOrders(account.ID, req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) RecreateOperationsForOrder(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputRecreateOperationsForOrder
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	err = s.Repository.RecreateOperationsForOrder(account.ID, &tempInput, req.UserID)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil

}

func (s *GRPCOperationServer) BalancesGroupedByToUID(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.OperationListFilters
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	data, err := s.Repository.BalancesGroupedByToUID(req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}

func (s *GRPCOperationServer) CalculateDaysCountForAllUIDs(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput []string
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	data, err := s.Repository.CalculateDaysCountForAllUIDs(req.AccountToken, &tempInput)
	if err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return &pb.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &pb.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonData}, nil

}
