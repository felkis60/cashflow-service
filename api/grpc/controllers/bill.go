package controllers

import (
	"gitlab.com/felkis60/cashflow-service/api/grpc/app"
	pb "gitlab.com/felkis60/cashflow-service/api/grpc/app"
	"gitlab.com/felkis60/cashflow-service/helpers"

	"context"
	"encoding/json"

	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"
)

type GRPCBillServer struct {
	Repository repository.BillRepository
	pb.BillServiceServer
}

func (s *GRPCBillServer) Create(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputCreateEditBill
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	bill, err := s.Repository.Create(&tempInput, account.ID)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonBill, err := json.Marshal(bill)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonBill}, nil
}

func (s *GRPCBillServer) Edit(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputCreateEditBill
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.Edit(req.UID, req.AccountToken, &tempInput)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonBill, err := json.Marshal(data)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonBill}, nil
}

func (s *GRPCBillServer) Delete(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {
	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	err = s.Repository.Delete(req.UID, req.AccountToken)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil
}

func (s *GRPCBillServer) Get(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	bill, err := s.Repository.GetByUID(req.UID, req.AccountToken)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonBill, err := json.Marshal(bill)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonBill}, nil
}

func (s *GRPCBillServer) List(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputListBill
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	bills, err := s.Repository.List(req.AccountToken, &tempInput)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonBills, err := json.Marshal(bills)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonBills}, nil
}
