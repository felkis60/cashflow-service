package controllers

import (
	"gitlab.com/felkis60/cashflow-service/api/grpc/app"
	pb "gitlab.com/felkis60/cashflow-service/api/grpc/app"
	"gitlab.com/felkis60/cashflow-service/helpers"

	"context"
	"encoding/json"

	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"

	"gorm.io/gorm"
)

type GRPCAccountServer struct {
	Repository repository.AccountRepository
	pb.AccountServiceServer
}

func middlewareGetAccount(accountToken string, db *gorm.DB) (*types.Account, error) {
	var rep = repository.AccountRepository{db}
	return rep.Get(accountToken)

}

func (s *GRPCAccountServer) Create(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	var tempInput types.InputCreateEditAccount
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var account types.Account
	err := s.Repository.Create(&account, &tempInput)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonAccount, err := json.Marshal(account)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonAccount}, nil
}

func (s *GRPCAccountServer) Edit(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	account, err := s.Repository.Get(req.UID)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputCreateEditAccount
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	err = s.Repository.Edit(account, &tempInput, req.UID)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonAccount, err := json.Marshal(account)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonAccount}, nil
}

func (s *GRPCAccountServer) Get(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {
	account, err := s.Repository.Get(req.AccountToken)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}
	jsonAccount, err := json.Marshal(account)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonAccount}, nil
}
