package controllers

import (
	"gitlab.com/felkis60/cashflow-service/api/grpc/app"
	pb "gitlab.com/felkis60/cashflow-service/api/grpc/app"
	"gitlab.com/felkis60/cashflow-service/helpers"
	"gitlab.com/felkis60/cashflow-service/types"

	"context"
	"encoding/json"

	"gitlab.com/felkis60/cashflow-service/repository"
)

type GRPCCategoryServer struct {
	Repository repository.CategoryRepository
	pb.CategoryServiceServer
}

func (s *GRPCCategoryServer) Create(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	account, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputCreateCategory
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	category, err := s.Repository.Create(&tempInput, account.ID)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonCategory, err := json.Marshal(category)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonCategory}, nil
}

func (s *GRPCCategoryServer) Edit(ctx context.Context, req *pb.InputEditByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputEditCategory
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	data, err := s.Repository.Edit(req.UID, req.AccountToken, &tempInput)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonCategory, err := json.Marshal(data)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonCategory}, nil
}

func (s *GRPCCategoryServer) Delete(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {
	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	err = s.Repository.Delete(req.UID, req.AccountToken)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: nil}, nil
}

func (s *GRPCCategoryServer) Get(ctx context.Context, req *pb.InputDoByUID) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	category, err := s.Repository.GetByUID(req.UID)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonCategory, err := json.Marshal(category)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonCategory}, nil
}

func (s *GRPCCategoryServer) List(ctx context.Context, req *pb.InputDoByJSON) (*pb.ServiceResponse, error) {

	_, err := middlewareGetAccount(req.AccountToken, s.Repository.Db)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	var tempInput types.InputGetListCategory
	if err := json.Unmarshal(req.Body, &tempInput); err != nil {
		return &pb.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	categories, err := s.Repository.List(req.AccountToken, &tempInput)
	if err != nil {
		return &app.ServiceResponse{Code: 400, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	jsonCategories, err := json.Marshal(categories)
	if err != nil {
		return &app.ServiceResponse{Code: 500, Message: err.Error()}, helpers.AddServiceNameToError(err)
	}

	return &app.ServiceResponse{Code: 200, Message: "Success!", Payload: jsonCategories}, nil
}
