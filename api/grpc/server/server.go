package main

import (
	"log"
	"net"
	"os"

	pb "gitlab.com/felkis60/cashflow-service/api/grpc/app"
	"gitlab.com/felkis60/cashflow-service/api/grpc/controllers"
	db "gitlab.com/felkis60/cashflow-service/database"
	start "gitlab.com/felkis60/cashflow-service/init"
	logs "gitlab.com/felkis60/cashflow-service/log"
	"gitlab.com/felkis60/cashflow-service/repository"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {

	start.SystemStartup(true, true, false)

	lis, err := net.Listen("tcp", ":"+os.Getenv("GRPCPORT"))
	if err != nil {
		log.Fatalf(logs.ERRListen, err)
	}

	s := grpc.NewServer()

	pb.RegisterAccountServiceServer(s, &controllers.GRPCAccountServer{Repository: repository.AccountRepository{Db: db.Db}})
	pb.RegisterCategoryServiceServer(s, &controllers.GRPCCategoryServer{Repository: repository.CategoryRepository{Db: db.Db}})
	pb.RegisterOperationServiceServer(s, &controllers.GRPCOperationServer{Repository: repository.OperationRepository{Db: db.Db}})
	pb.RegisterBillServiceServer(s, &controllers.GRPCBillServer{Repository: repository.BillRepository{Db: db.Db}})

	log.Printf(logs.INFStartServer, "grcp")
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf(logs.ERRServe, err)
	}

}
