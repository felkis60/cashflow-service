package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	routes "gitlab.com/felkis60/cashflow-service/api/rest/router"
	db "gitlab.com/felkis60/cashflow-service/database"
	start "gitlab.com/felkis60/cashflow-service/init"
	"gitlab.com/felkis60/cashflow-service/log"
	logs "gitlab.com/felkis60/cashflow-service/log"
	types "gitlab.com/felkis60/cashflow-service/types"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	conv "github.com/smartystreets/goconvey/convey"
)

var router *gin.Engine

const rightToken = "123"
const rightToken2 = "1234"

var inited = false

func makeRequest(method string, url string, accountToken string, body interface{}, additionalHeaders *map[string]string) (int, *types.RestRespone) {
	var resp types.RestRespone
	w := httptest.NewRecorder()

	var marshaledBody []byte
	var req *http.Request
	var err error
	if body != nil {
		marshaledBody, err = json.Marshal(body)
		if err != nil {
			return 0, nil
		}
		req, err = http.NewRequest(method, url, bytes.NewReader(marshaledBody))
		if err != nil {
			return 0, nil
		}
	} else {
		req, err = http.NewRequest(method, url, nil)
		if err != nil {
			return 0, nil
		}
	}
	req.Header.Set("Token", accountToken)
	if additionalHeaders != nil {
		for key, value := range *additionalHeaders {
			req.Header.Set(key, value)
		}
	}
	router.ServeHTTP(w, req)
	err = json.Unmarshal(w.Body.Bytes(), &resp)
	if err != nil {
		return 0, nil
	}

	return w.Code, &resp
}

func setup() {
	if !inited {

		start.SystemStartup(false, true, false)
		router = routes.SetupRouter()
		inited = true

	}

	var result int
	db.Db.Raw("DELETE FROM accounts").Scan(&result)
	db.Db.Raw("DELETE FROM bills").Scan(&result)
	db.Db.Raw("DELETE FROM categories").Scan(&result)
	db.Db.Raw("DELETE FROM operations").Scan(&result)

	db.Db.Create(&types.Account{
		Name:  "test",
		Token: rightToken})

	db.Db.Create(&types.Account{
		Name:  "test2",
		Token: rightToken2})

}

func TestAll(t *testing.T) {

	conv.Convey("Cashflow Service Tests", t, func() {

		setup()

		conv.Convey("Bill", func() {
			conv.Convey("Create in different accounts with same code", func() {
				var tempName = "temp"
				var tempCode = "code"
				code, _ := makeRequest("POST", "/v1/bills/", rightToken, &types.InputCreateEditBill{Name: &tempName, Code: &tempCode}, nil)
				conv.So(code, conv.ShouldEqual, 200)

				code, _ = makeRequest("POST", "/v1/bills/", rightToken2, &types.InputCreateEditBill{Name: &tempName, Code: &tempCode}, nil)
				conv.So(code, conv.ShouldEqual, 200)
			})

			conv.Convey("Try create with same code under same account, must fail", func() {
				var tempName = "temp"
				var tempCode = "code"
				code, resp := makeRequest("POST", "/v1/bills/", rightToken, &types.InputCreateEditBill{Name: &tempName, Code: &tempCode}, nil)
				conv.So(code, conv.ShouldEqual, 200)

				code, resp = makeRequest("POST", "/v1/bills/", rightToken, &types.InputCreateEditBill{Name: &tempName, Code: &tempCode}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRBillCodeAlreadyExists)
			})

			conv.Convey("Try create with errors, must fail", func() {

				var tempName = "temp"
				var tempCode = "code"
				code, resp := makeRequest("POST", "/v1/bills/", rightToken, &types.InputCreateEditBill{Name: &tempName}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRBillCodeReq)

				code, resp = makeRequest("POST", "/v1/bills/", rightToken, &types.InputCreateEditBill{Code: &tempCode}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRBillNameReq)

			})
		})

		conv.Convey("Operations", func() {

			conv.Convey("Create in different accounts", func() {
				var tempDate = time.Now()
				var tempUID1 = uuid.Must(uuid.NewV4()).String()
				var tempSource = "vehicle"
				code, resp := makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 200)

				code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 200)

				code, _ = makeRequest("POST", "/v1/operations/", rightToken2, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 200)

				conv.Convey("On 1 account must be 2000rub, on 2 account must be 1000rub", func() {
					code, resp = makeRequest("POST", "/v1/operations/summ", rightToken, &types.InputGetListOperations{Filters: types.OperationListFilters{ToUID: &tempUID1}}, nil)
					conv.So(code, conv.ShouldEqual, 200)
					conv.So(resp.Payload.(float64), conv.ShouldEqual, 2000.0)

					code, resp = makeRequest("POST", "/v1/operations/summ", rightToken2, &types.InputGetListOperations{Filters: types.OperationListFilters{ToUID: &tempUID1}}, nil)
					conv.So(code, conv.ShouldEqual, 200)
					conv.So(resp.Payload.(float64), conv.ShouldEqual, 1000.0)

				})

			})

			//conv.Convey("Create Operation with reverse Operation", func() {
			//	var tempDate = time.Now()
			//	var tempUID1 = uuid.Must(uuid.NewV4()).String()
			//	var tempUID2 = uuid.Must(uuid.NewV4()).String()
			//	var tempSource = "vehicle"
			//	code, _ := makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, FromUID: &tempUID2, FromSource: &tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
			//	conv.So(code, conv.ShouldEqual, 200)
			//
			//	var opeartions []types.Operation
			//	db.Db.Find(&opeartions)
			//
			//	conv.So(len(opeartions), conv.ShouldEqual, 2)
			//	conv.So(opeartions[0].Amount, conv.ShouldEqual, opeartions[1].Amount*-1)
			//	conv.So(opeartions[0].Date, conv.ShouldEqual, opeartions[1].Date)
			//	conv.So(opeartions[0].ToSource, conv.ShouldEqual, opeartions[1].FromSource)
			//	conv.So(opeartions[0].FromSource, conv.ShouldEqual, opeartions[1].ToSource)
			//	conv.So(opeartions[0].FromUID, conv.ShouldEqual, opeartions[1].ToUID)
			//	conv.So(opeartions[0].ToUID, conv.ShouldEqual, opeartions[1].FromUID)
			//	conv.So(opeartions[0].CategoriesID, conv.ShouldEqual, opeartions[1].CategoriesID)
			//	conv.So(opeartions[0].UID, conv.ShouldNotEqual, opeartions[1].UID)
			//
			//})

			conv.Convey("Try amount of deposite negative, must fail", func() {
				var tempDate = time.Now()
				var tempUID1 = uuid.Must(uuid.NewV4()).String()
				var tempDepositeTrue = true
				var tempSource = "vehicle"
				code, resp := makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: -1.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT", Deposit: &tempDepositeTrue}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRNegativeDeposit)

				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 100.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT", Deposit: &tempDepositeTrue}, nil)
				conv.So(code, conv.ShouldEqual, 200)

				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: -100.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT", Deposit: &tempDepositeTrue}, nil)
				conv.So(code, conv.ShouldEqual, 200)

				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: -100.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT", Deposit: &tempDepositeTrue}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRNegativeDeposit)

			})

			conv.Convey("Try create with errors, must fail", func() {
				var tempDate = time.Now()
				var tempUID1 = uuid.Must(uuid.NewV4()).String()
				var tempUID2 = uuid.Must(uuid.NewV4()).String()
				var tempSource = "vehicle"
				var tempWrongSource = "elden ring"
				var tempRelationsUIDs3 = []string{"weruc293r-2", "f20fjm029mjf0923", "f023nu01xww"}
				var tempRelationsUIDs2 = []string{"weruc293r-2", "f023nu01xww"}
				var tempRelationsSources2 = []string{"vehicle", "user"}
				var tempWrongRelationsSources2 = []string{"vehicle", "useeeeer"}

				//No ToUID
				code, resp := makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRToUIDReq)

				//No ToSource
				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRToSourceReq)

				//Wrong CashType
				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CAAAAAAASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRWrongOperationCashType)

				//Wrong Status
				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAAAAAAAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRWrongOperationStatus)

				//No Amount
				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ErrAmountReq)

				//Amount == 0
				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 0.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ErrAmountReq)

				//Wrong ToSource
				code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempWrongSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)

				//RelationUIDs and no RelationsSources
				code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, RelationsUIDs: &tempRelationsUIDs3, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)

				//RelationUIDs and RelationsSources different len
				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, RelationsUIDs: &tempRelationsUIDs3, RelationsSources: &tempRelationsSources2, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, logs.ERRRelationsSameElements)

				//Wrong RelationsSources
				code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, RelationsUIDs: &tempRelationsUIDs2, RelationsSources: &tempWrongRelationsSources2, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)

				//FromUID and no FromSource
				code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, FromUID: &tempUID2, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)

				//ToUID == FromUID
				code, resp = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, FromUID: &tempUID1, FromSource: &tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)
				conv.So(resp.Message, conv.ShouldEqual, log.ERRSameUID)

				//Wrong FromSource
				code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, FromUID: &tempUID2, FromSource: &tempWrongSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
				conv.So(code, conv.ShouldEqual, 400)

			})

			//conv.Convey("Create Operation 1, 1000rub to Bill 1", func() {
			//	var tempDate = time.Now()
			//	var tempUID1 = uuid.Must(uuid.NewV4()).String()
			//	var tempUID2 = uuid.Must(uuid.NewV4()).String()
			//	var tempSource = "vehicle"
			//	code, resp := makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID1, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
			//	conv.So(code, conv.ShouldEqual, 200)
			//
			//	conv.Convey("Create Operation 2, 1000rub to Bill 2", func() {
			//		code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 1000.0, ToUID: tempUID2, ToSource: tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
			//		conv.So(code, conv.ShouldEqual, 200)
			//
			//		conv.Convey("Create Operation 3, 500rub from Bill 1 to Bill 2", func() {
			//			code, _ = makeRequest("POST", "/v1/operations/", rightToken, &types.InputCreateOperation{Amount: 500.0, FromUID: &tempUID1, ToUID: tempUID2, ToSource: tempSource, FromSource: &tempSource, Date: &tempDate, CashType: "CASH", Status: "DEFAULT"}, nil)
			//			conv.So(code, conv.ShouldEqual, 200)
			//
			//			conv.Convey("On Bill 1 must be 500rub, on Bill 2 must be 1500rub", func() {
			//				code, resp = makeRequest("POST", "/v1/operations/summ", rightToken, &types.InputGetListOperations{Filters: types.OperationListFilters{ToUID: &tempUID1}}, nil)
			//				conv.So(code, conv.ShouldEqual, 200)
			//				conv.So(resp.Payload.(float64), conv.ShouldEqual, 500.0)
			//
			//				code, resp = makeRequest("POST", "/v1/operations/summ", rightToken, &types.InputGetListOperations{Filters: types.OperationListFilters{ToUID: &tempUID2}}, nil)
			//				conv.So(code, conv.ShouldEqual, 200)
			//				conv.So(resp.Payload.(float64), conv.ShouldEqual, 1500.0)
			//
			//			})
			//		})
			//	})
			//})

		})

	})

}
