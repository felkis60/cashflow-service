package controllers

import (
	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"

	"github.com/gin-gonic/gin"
)

type CategoryController struct {
	Repository repository.CategoryRepository
}

//
// @Summary Create new category
// @Tags Categories
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_     body types.InputCreateCategory true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/category/ [post]
func (ctrl *CategoryController) Create(c *gin.Context) {
	var input types.InputCreateCategory
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Delete category
// @Tags Categories
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Category UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/categories/{uid}/delete [post]
func (ctrl *CategoryController) Delete(c *gin.Context) {

	if err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string)); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

//
// @Summary Edit category
// @Tags Categories
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body types.InputEditCategory true "Request body"
// @Param uid path string true "Category UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/categories/{uid}/edit [post]
func (ctrl *CategoryController) Edit(c *gin.Context) {
	var input types.InputEditCategory
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: data})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})

}

//
// @Summary Get categories
// @Tags Categories
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body types.InputGetListCategory false "Request body"
// @Success 200 {object} []types.Category
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/categories/list [post]
func (ctrl *CategoryController) List(c *gin.Context) {

	var input types.InputGetListCategory
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Get one category
// @Tags Categories
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Category UID"
// @Success 200 {object} types.Category
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/categories/{uid} [post]
func (ctrl *CategoryController) GetByUID(c *gin.Context) {

	data, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}
