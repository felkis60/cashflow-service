package controllers

import (
	"gitlab.com/felkis60/cashflow-service/repository"
	t "gitlab.com/felkis60/cashflow-service/types"

	"github.com/gin-gonic/gin"
)

type AccountController struct {
	Repository repository.AccountRepository
}

//
// @Summary Create new account
// @Tags Accounts
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body t.InputCreateEditAccount true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/accounts/ [post]
func (ctrl *AccountController) Create(c *gin.Context) {
	var input t.InputCreateEditAccount
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, t.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	var account t.Account

	if err := ctrl.Repository.Create(&account, &input); err != nil {
		c.AbortWithStatusJSON(400, t.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, t.RestRespone{Message: "Success!", Payload: account})
}

//
// @Summary Edit account
// @Tags Accounts
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ 	body t.InputCreateEditAccount true "Request body"
// @Param Token path string true "Account Token"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/accounts/{token}/edit [post]
func (ctrl *AccountController) Edit(c *gin.Context) {
	token := c.Param("token")
	var input t.InputCreateEditAccount
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, t.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	account, err := ctrl.Repository.Get(token)
	if err != nil {
		c.JSON(400, t.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	if err := ctrl.Repository.Edit(account, &input, token); err != nil {
		c.JSON(400, t.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, t.RestRespone{Message: "Success!", Payload: account})
}

//
// @Summary Get one account
// @Tags Accounts
// @Description
// @Accept  json
// @Produce  json
// @Param Token path string true "Account Token"
// @Success 200 {object} t.Account
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/accounts/{token} [post]
func (ctrl *AccountController) Get(c *gin.Context) {
	token := c.Param("token")

	account, err := ctrl.Repository.Get(token)
	if err != nil {
		c.JSON(400, t.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, t.RestRespone{Message: "Success!", Payload: account})
}
