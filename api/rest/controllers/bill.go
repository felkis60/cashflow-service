package controllers

import (
	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"

	"github.com/gin-gonic/gin"
)

type BillController struct {
	Repository repository.BillRepository
}

//
// @Summary Create new bill
// @Tags Bills
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_     body types.InputCreateEditBill true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/bills/ [post]
func (ctrl *BillController) Create(c *gin.Context) {
	var input types.InputCreateEditBill
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Delete bill
// @Tags Bills
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Bill UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/bills/{uid}/delete [post]
func (ctrl *BillController) Delete(c *gin.Context) {

	if err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string)); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

//
// @Summary Edit bill
// @Tags Bills
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ 	body types.InputCreateEditBill true "Request body"
// @Param uid path string true "Bill UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/bills/{uid}/edit [post]
func (ctrl *BillController) Edit(c *gin.Context) {
	var input types.InputCreateEditBill
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Edit(c.MustGet("uid").(string), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: data})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})

}

//
// @Summary Get one bill
// @Tags Bills
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Bill UID"
// @Success 200 {object} types.Bill
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/bills/{uid} [post]
func (ctrl *BillController) GetByUID(c *gin.Context) {

	data, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

//
// @Summary Get bills
// @Tags Bills
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body types.InputListBill false "Request body"
// @Success 200 {object} types.Bill
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/bills/list [post]
func (ctrl *BillController) List(c *gin.Context) {

	var input types.InputListBill
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.List(c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}
