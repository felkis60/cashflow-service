package controllers

import (
	"gitlab.com/felkis60/cashflow-service/repository"
	"gitlab.com/felkis60/cashflow-service/types"

	"github.com/gin-gonic/gin"
)

type OperationController struct {
	Repository repository.OperationRepository
}

// @Summary Create new operation
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_     body types.InputCreateOperation true "Request body"
// @Success 200 {object} types.Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/ [post]
func (ctrl *OperationController) Create(c *gin.Context) {
	var input types.InputCreateOperation
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Create(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

func (ctrl *OperationController) CreateMultiple(c *gin.Context) {
	var input []types.InputCreateOperation
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.CreateMultiple(&input, c.MustGet("account_id").(int64), 0)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Delete operation
// @Tags Operations
// @Description
// @Accept  json
// @Produce json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Operation UID"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/{uid}/delete [post]
func (ctrl *OperationController) Delete(c *gin.Context) {

	if err := ctrl.Repository.Delete(c.MustGet("uid").(string), c.MustGet("token").(string), 0); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	c.JSON(200, types.RestRespone{Message: "Success!", Payload: nil})
}

// @Summary Get one operation
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param uid path string true "Operation UID"
// @Success 200 {object} types.Operation
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/{uid} [post]
func (ctrl *OperationController) GetByUID(c *gin.Context) {

	data, err := ctrl.Repository.GetByUID(c.MustGet("uid").(string), c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get operations
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body types.InputGetListOperations false "Request body"
// @Success 200 {object} []types.Operation []types.Category
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/list [post]
func (ctrl *OperationController) List(c *gin.Context) {

	var input types.InputGetListOperations
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.List(&input, c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get bill balances for each day in interval
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body types.InputBillBalanceByDays false "Request body"
// @Success 200 {object} map[int64]map[string]float64
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/bill-balances-each-day [post]
func (ctrl *OperationController) BalanceByDaysForEachBill(c *gin.Context) {

	var input types.InputBillBalanceByDays
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.BalanceByDaysForEachBill(&input, c.MustGet("token").(string))
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Get summ of operations amount
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body types.OperationListFilters false "Request body"
// @Success 200 {object} float64
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/summ [post]
func (ctrl *OperationController) Summ(c *gin.Context) {

	var input types.OperationListFilters
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	data, err := ctrl.Repository.Summ(c.MustGet("account_id").(int64), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

// @Summary Analytics
// @Tags Operations
// @Description
// @Accept  json
// @Produce  json
// @Param 	Token header string true "Account Token"
// @Param 	_ body types.OperationListFilters false "Request body"
// @Success 200 {object} []types.Analytics []types.Category
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/operations/analytics [post]
func (ctrl *OperationController) Analytics(c *gin.Context) {
	var input types.OperationListFilters
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.Analytics(c.MustGet("account_id").(int64), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}

func (ctrl *OperationController) CreateFromRex(c *gin.Context) {
	var input types.InputCreateFromRex
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	data, err := ctrl.Repository.CreateFromRex(c.MustGet("account_id").(int64), c.MustGet("token").(string), &input)
	if err != nil {
		c.JSON(400, types.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, types.RestRespone{Message: "Success!", Payload: data})
}
