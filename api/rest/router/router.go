package other

import (
	controller "gitlab.com/felkis60/cashflow-service/api/rest/controllers"
	middleware "gitlab.com/felkis60/cashflow-service/api/rest/middleware"
	db "gitlab.com/felkis60/cashflow-service/database"
	repos "gitlab.com/felkis60/cashflow-service/repository"

	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("/v1")
	{
		{
			accounts := v1.Group("/accounts")
			rep := repos.AccountRepository{Db: db.Db}
			ctrl := &controller.AccountController{Repository: rep}

			accounts.POST("", ctrl.Create)
			accounts.POST("/", ctrl.Create)
			accounts.POST("/:token", ctrl.Get)
			accounts.POST("/:token/edit", ctrl.Edit)
		}

		{
			operations := v1.Group("/operations").Use(middleware.TokenLogin())

			rep := repos.OperationRepository{Db: db.Db}
			ctrl := &controller.OperationController{Repository: rep}

			operations.POST("", ctrl.Create)
			operations.POST("/", ctrl.Create)
			//operations.POST("/multiple", ctrl.CreateMultiple)
			operations.POST("/list", ctrl.List)
			operations.POST("/summ", ctrl.Summ)
			operations.POST("/analytics", ctrl.Analytics)
			operations.POST("/create-from-rex", ctrl.CreateFromRex)
			operations.POST("/bill-balances-each-day", ctrl.BalanceByDaysForEachBill)

			{
				operationsUID := operations.Use(middleware.UIDHandler())

				operationsUID.POST("/:uid", ctrl.GetByUID)
				operationsUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			bills := v1.Group("/bills").Use(middleware.TokenLogin())

			rep := repos.BillRepository{Db: db.Db}
			ctrl := &controller.BillController{Repository: rep}

			bills.POST("", ctrl.Create)
			bills.POST("/", ctrl.Create)
			bills.POST("/list", ctrl.List)

			{
				billsUID := bills.Use(middleware.UIDHandler())

				billsUID.POST("/:uid", ctrl.GetByUID)
				billsUID.POST("/:uid/edit", ctrl.Edit)
				billsUID.POST("/:uid/delete", ctrl.Delete)
			}
		}

		{
			categories := v1.Group("/categories").Use(middleware.TokenLogin())

			rep := repos.CategoryRepository{Db: db.Db}
			ctrl := &controller.CategoryController{Repository: rep}

			categories.POST("", ctrl.Create)
			categories.POST("/", ctrl.Create)
			categories.POST("/list", ctrl.List)

			{
				categoriesUID := categories.Use(middleware.UIDHandler())

				categoriesUID.POST("/:uid", ctrl.GetByUID)
				categoriesUID.POST("/:uid/edit", ctrl.Edit)
				categoriesUID.POST("/:uid/delete", ctrl.Delete)
			}
		}
	}

	return r
}
