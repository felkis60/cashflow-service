package middleware

import (
	db "gitlab.com/felkis60/cashflow-service/database"
	logs "gitlab.com/felkis60/cashflow-service/log"
	t "gitlab.com/felkis60/cashflow-service/types"

	"github.com/gin-gonic/gin"
)

func TokenLogin() gin.HandlerFunc {
	return func(c *gin.Context) {
		var token t.InputAccountTokenHeader
		if err := c.ShouldBindHeader(&token); err != nil {
			c.AbortWithStatusJSON(400, gin.H{"error": err})
			return
		}

		if token.Token == "" {
			c.AbortWithStatusJSON(400, gin.H{"error": logs.ERRTokenReq})
			return
		}

		var account t.Account
		if result := db.Db.First(&account, "token = ?", token.Token); result.RowsAffected == 0 {
			c.AbortWithStatusJSON(400, gin.H{"error": logs.ERRNoSuchToken})
			return
		}

		c.Set("token", token.Token)
		c.Set("account_id", account.ID)

		c.Next()
	}
}

func UIDHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		uid := c.Param("uid")

		if uid == "" {
			c.AbortWithStatusJSON(400, gin.H{"error": logs.ERRUIDReq})
		}

		c.Set("uid", uid)

		c.Next()

	}
}
