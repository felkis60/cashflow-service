package main

import (
	"log"
	"net/http"
	"os"

	r "gitlab.com/felkis60/cashflow-service/api/rest/router"
	s "gitlab.com/felkis60/cashflow-service/init"
	l "gitlab.com/felkis60/cashflow-service/log"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "gitlab.com/felkis60/cashflow-service/api/rest/server/docs"

	socketio "github.com/googollee/go-socket.io"
)

func main() {
	s.SystemStartup(true, true, true)

	server := socketio.NewServer(nil)

	server.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")
		log.Println("connected:", s.ID())
		return nil
	})

	server.OnError("/", func(s socketio.Conn, e error) {
		log.Println("meet error:", e)
	})

	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		log.Println("closed", reason)
	})

	//server.OnEvent("/", "created-vehicle", func(s socketio.Conn, data []byte) {
	//	log.Println("notice: 1")
	//	var tempJson map[string]interface{}
	//
	//	if err := json.Unmarshal(data, &tempJson); err != nil {
	//		log.Print(err)
	//		return
	//	}
	//	log.Println(tempJson)
	//})

	go func() {
		if err := server.Serve(); err != nil {
			log.Fatalf("socketio listen error: %s\n", err)
		}
	}()
	defer server.Close()

	router := r.SetupRouter()

	router.GET("/socket.io/*any", gin.WrapH(server))
	router.POST("/socket.io/*any", gin.WrapH(server))
	router.StaticFS("/public", http.Dir("../asset"))
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	log.Printf(l.INFStartServer, "rest")
	router.Run(":" + os.Getenv("RESTPORT"))

}
