package main

import (
	"log"
	"os/exec"
	"sync"

	startup "gitlab.com/felkis60/cashflow-service/init"
)

// @title Cashflow Service
// @version 0.1

func main() {
	var wg sync.WaitGroup

	//cmd1 := exec.Command("protoc", "--go_out=.", "--go_opt=paths=source_relative", "api/grpc/app/app.proto")
	//cmd2 := exec.Command("protoc", "--go-grpc_out=.", "--go-grpc_opt=paths=source_relative", "api/grpc/app/app.proto")
	cmd3 := exec.Command("swag", "init", "-o", "./api/rest/server/docs")
	cmd4 := exec.Command("go", "test", "./api/rest/test/.")
	cmd7 := exec.Command("go", "run", "./api/rest/server/.")
	cmd8 := exec.Command("go", "run", "./api/grpc/server/.")

	//if _, err := cmd1.Output(); err != nil {
	//	log.Fatalf(err.Error())
	//}
	//if _, err := cmd2.Output(); err != nil {
	//	log.Fatalf(err.Error())
	//}
	if _, err := cmd3.Output(); err != nil {
		log.Fatalf(err.Error())
	}
	if _, err := cmd4.Output(); err != nil {
		log.Fatalf(err.Error())
	}

	startup.SystemStartup(true, true, false)

	wg.Add(1)
	go cmd7.Output()

	wg.Add(1)
	go cmd8.Output()

	wg.Wait()
}
