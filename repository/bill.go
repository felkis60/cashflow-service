package repository

import (
	"errors"
	"log"
	"math"
	"time"

	db "gitlab.com/felkis60/cashflow-service/database"
	tools "gitlab.com/felkis60/cashflow-service/helpers"
	logs "gitlab.com/felkis60/cashflow-service/log"
	types "gitlab.com/felkis60/cashflow-service/types"
	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

const (
	cacheLifetime = time.Hour * 6
	billTableName = "bills"
)

type BillRepository struct {
	Db *gorm.DB
}

func writeBill(bill *types.Bill, input *types.InputCreateEditBill) error {
	if input.ExtID != nil {
		bill.ExtID = *input.ExtID
	}
	if input.Name != nil {
		bill.Name = *input.Name
	}
	if input.Code != nil {
		bill.Code = *input.Code
	}
	if input.CashType != nil {
		bill.CashType = *input.CashType
		if bill.CashType != "" {
			switch bill.CashType {
			case "SETTLEMENT":
			case "CASH":
			default:
				return errors.New("ERROR: wrong cash_type in bill")
			}
		}
	}

	if bill.Name == "" {
		return errors.New(logs.ERRBillNameReq)
	}
	if bill.Code == "" {
		return errors.New(logs.ERRBillCodeReq)
	}
	return nil
}

func (rep *BillRepository) AddBalance(bill *types.Bill) error {
	if db.RedisDB != nil {
		var cacheKey = tools.GenerateBillBalanceCacheKey(bill.ID)
		balance, err := db.RedisDB.Get(db.RedisContext, cacheKey).Float64()
		if err != nil {
			if result := rep.Db.Table("operations").Select("COALESCE(SUM (amount), 0)").Where("operations.to_bills_id = ?", bill.ID).Where("operations.deleted_at IS NULL AND operations.status != 'FAILED'").Scan(&balance); result.Error != nil {
				log.Print(result.Error)
				return result.Error
			}
			db.RedisDB.Set(db.RedisContext, cacheKey, balance, time.Hour*24*100)
		}
		bill.Balance = balance
	}
	return errors.New("ERROR: redis is not inited for addBalance")
}

func (rep *BillRepository) Create(input *types.InputCreateEditBill, accountID int64) (*types.Bill, error) {

	var bill types.Bill
	err := writeBill(&bill, input)
	if err != nil {
		return nil, err
	}

	var existingBills []types.Bill
	if result := rep.Db.Find(&existingBills, "accounts_id = ? AND code = ?", accountID, bill.Code); result.RowsAffected != 0 {
		return nil, errors.New(logs.ERRBillCodeAlreadyExists)
	}

	bill.UID = uuid.Must(uuid.NewV4()).String()
	bill.AccountsID = accountID

	rep.Db.Create(&bill)

	return &bill, nil
}

func (rep *BillRepository) Edit(UID string, accountToken string, input *types.InputCreateEditBill) (*types.Bill, error) {

	data, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return nil, err
	}

	if err := writeBill(data, input); err != nil {
		return nil, err
	}

	var existingBill types.Bill
	if result := rep.Db.Limit(1).Find(&existingBill, "accounts_id = ? AND code = ? AND id != ?", data.AccountsID, data.Code, data.ID); result.RowsAffected != 0 {
		return nil, errors.New(logs.ERRBillCodeAlreadyExists)
	}

	rep.Db.Save(data)

	return data, nil
}

func (rep *BillRepository) Delete(UID string, accountToken string) error {

	data, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return err
	}

	rep.Db.Delete(data)

	return nil
}

func (rep *BillRepository) GetByUID(UID string, accountToken string) (*types.Bill, error) {
	var data types.Bill

	if result := helpers.DbAccountLeftJoin(billTableName, accountToken, rep.Db).Find(&data, "bills.uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchBillUID)
	}
	rep.AddBalance(&data)

	return &data, nil
}

func (rep *BillRepository) GetByID(ID int64, accountToken string) (*types.Bill, error) {
	var data types.Bill
	if result := helpers.DbAccountLeftJoin(billTableName, accountToken, rep.Db).Find(&data, ID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchBillID)
	}
	//rep.AddBalance(&data)

	return &data, nil
}

func (rep *BillRepository) List(accountToken string, input *types.InputListBill) (*types.Pagination, error) {

	var tx1 = helpers.DbAccountLeftJoin(billTableName, accountToken, rep.Db.Model(&types.Bill{}))

	tx1 = tx1.Session(&gorm.Session{})

	//Count
	var totalItems int64
	tx1.Count(&totalItems)

	//List
	tx1 = helpers.DbSetOrder("bills", input.OrderBy, input.OrderDir)(tx1)

	var data []types.Bill
	if result := helpers.Paginate(&input.Page, &input.PageSize)(tx1).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var totalPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	for i := range data {
		rep.AddBalance(&data[i])
	}

	return &types.Pagination{Page: &input.Page, Items: data, TotalItems: &totalItems, TotalPages: &totalPages}, nil

}
