package repository

import (
	"errors"
	"math"
	"strings"

	logs "gitlab.com/felkis60/cashflow-service/log"
	"gitlab.com/felkis60/cashflow-service/types"
	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type CategoryRepository struct {
	Db *gorm.DB
}

const (
	categoriesTableName = "categories"
)

func checkCategory(category *types.Category) error {

	if category.Name == "" {
		return errors.New(logs.ERRBillNameReq)
	}
	if category.OperationType == "" {
		return errors.New(logs.ERRBillCodeReq)
	}
	if category.ParentID != 0 && category.ParentID == category.ID {
		return errors.New(logs.ERRParentToYourself)
	}
	if category.OperationType != "CREDIT" && category.OperationType != "DEBIT" {
		return errors.New(logs.ERRWrongCategoryOperationType)
	}
	return nil
}

func (rep *CategoryRepository) Create(input *types.InputCreateCategory, accountID int64) (*types.Category, error) {

	return rep.AdminCreate(input, accountID, 0)
}

func (rep *CategoryRepository) AdminCreate(input *types.InputCreateCategory, accountID int64, id int64) (*types.Category, error) {

	var category types.Category

	category.ID = id
	category.UID = uuid.Must(uuid.NewV4()).String()
	category.AccountsID = accountID
	category.ExtID = input.ExtID
	category.Name = input.Name
	category.OperationType = strings.ToUpper(input.OperationType)
	category.Description = input.Description

	if input.ParentUID != nil {
		var existingCategory types.Category
		if result := rep.Db.Find(&existingCategory, "uid = ?", *input.ParentUID); result.RowsAffected == 0 {
			return nil, errors.New(logs.ERRNoSuchCategoryUID)
		}
		category.ParentID = existingCategory.ID
	}

	if err := checkCategory(&category); err != nil {
		return nil, err
	}

	rep.Db.Create(&category)

	return &category, nil
}

func (rep *CategoryRepository) Edit(UID string, accountToken string, input *types.InputEditCategory) (*types.Category, error) {

	data, err := rep.GetByUID(UID)
	if err != nil {
		return nil, err
	}

	if input.ExtID != nil {
		data.ExtID = *input.ExtID
	}
	if input.Name != nil {
		data.Name = *input.Name
	}
	if input.OperationType != nil {
		data.OperationType = *input.OperationType
	}
	if input.Description != nil {
		data.Description = *input.Description
	}
	if input.ParentUID != nil {
		var existingCategory types.Category
		if result := rep.Db.Find(&existingCategory, "uid = ?", *input.ParentUID); result.RowsAffected == 0 {
			return nil, errors.New(logs.ERRNoSuchCategoryUID)
		}
		data.ParentID = existingCategory.ID
	}

	if err := checkCategory(data); err != nil {
		return nil, err
	}

	rep.Db.Save(data)

	return data, nil
}

func (rep *CategoryRepository) Delete(UID string, accountToken string) error {

	data, err := rep.GetByUID(UID)
	if err != nil {
		return err
	}

	rep.Db.Delete(data)

	return nil
}

func (rep *CategoryRepository) List(accountToken string, input *types.InputGetListCategory) (*types.Pagination, error) {

	var tx1 = rep.Db.Model(&types.Category{})
	if input.Filters != nil {
		if input.Filters.OperationType != nil {
			tx1 = tx1.Where("operation_type ILIKE ?", *input.Filters.OperationType)
		}
	}

	tx1 = tx1.Session(&gorm.Session{})

	//Count
	var totalItems int64
	tx1.Count(&totalItems)

	//List
	tx1 = helpers.DbSetOrder("categories", input.OrderBy, input.OrderDir)(tx1)

	var data []types.Category
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 1000)(tx1).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var totalPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	return &types.Pagination{Page: &input.Page, Items: data, TotalItems: &totalItems, TotalPages: &totalPages}, nil
}

func (rep *CategoryRepository) GetByUID(UID string) (*types.Category, error) {
	var data types.Category
	if result := rep.Db.First(&data, "uid = ?", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchCategoryUID)
	}
	return &data, nil
}
