package repository

import (
	"encoding/json"
	"errors"
	"log"
	"math"
	"strconv"
	"strings"
	"time"

	db "gitlab.com/felkis60/cashflow-service/database"
	tools "gitlab.com/felkis60/cashflow-service/helpers"
	history "gitlab.com/felkis60/cashflow-service/helpers/history"

	logs "gitlab.com/felkis60/cashflow-service/log"
	types "gitlab.com/felkis60/cashflow-service/types"
	helpers "gitlab.com/felkis60/rex-microservices-helpers"

	helpers2 "gitlab.com/felkis60/rex-microservices-helpers"

	"github.com/gofrs/uuid"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

const redisTimeExpire = time.Hour * 6

type OperationRepository struct {
	Db *gorm.DB
}

func (rep *OperationRepository) deleteCacheForOperation(operation *types.Operation) {
	rep.deleteCache(operation.ToBillsID, operation.ToUID, operation.ToSource)
}

func (rep *OperationRepository) deleteCache(billId *int64, uid, source string) {

	if db.RedisDB != nil {
		if billId != nil {
			db.RedisDB.Del(db.RedisContext, tools.GenerateBillBalanceCacheKey(*billId))
		}

		switch source {

		case "ransom_order":
			db.RedisDB.Del(db.RedisContext, helpers.GenerateRedisCacheKeyForAllRansOrder(uid))

		case "rent_order":
			db.RedisDB.Del(db.RedisContext, helpers.GenerateRedisCacheKeyForSearchRentOrder(uid))

		case "vehicle":
		case "user":

		}
	}
}

func (rep *OperationRepository) checkOperation(operation *types.Operation) error {

	if !types.CheckOperationStatus(operation.Status) {
		return errors.New(logs.ERRWrongOperationStatus)
	}
	if !types.CheckOperationCashType(operation.CashType) {
		return errors.New(logs.ERRWrongOperationCashType)
	}

	if operation.ToUID == "" {
		return errors.New(logs.ERRToUIDReq)
	}
	if operation.ToSource == "" {
		return errors.New(logs.ERRToSourceReq)
	}
	if !types.CheckOperationSource(operation.ToSource) {
		return errors.New("ERROR: '" + operation.ToSource + "' source is not allowed")
	}
	if operation.Amount == 0 {
		return errors.New(logs.ErrAmountReq)
	}
	if operation.Date.IsZero() {
		return errors.New(logs.ErrDateReq)
	}
	if operation.FromUID != "" || operation.FromSource != "" {
		if operation.FromUID == "" {
			return errors.New("ERROR: 'from_uid' is required when 'from_table' is used")
		}
		if operation.FromSource == "" {
			return errors.New("ERROR: 'from_table' is required when 'from_uid' is used")
		}
		if !types.CheckOperationSource(operation.FromSource) {
			return errors.New("ERROR: '" + operation.FromSource + "' source is not allowed")
		}
		//if operation.FromSource == "bill" {
		//	var bills []types.Bill
		//	if result := rep.Db.Model(&types.Bill{}).Find(&bills, "bills.uid = ?", operation.FromUID); result.Error != nil {
		//		return result.Error
		//	} else if result.RowsAffected <= 0 {
		//		return errors.New("ERROR: no such bill's uid")
		//	}
		//}
	}

	//if operation.ToSource == "bill" {
	//	var bills []types.Bill
	//	if result := rep.Db.Model(&types.Bill{}).Find(&bills, "bills.uid = ?", operation.ToUID); result.Error != nil {
	//		return result.Error
	//	} else if result.RowsAffected <= 0 {
	//		return errors.New("ERROR: no such bill's uid")
	//	}
	//}

	return nil
}

func (rep *OperationRepository) CreateMultiple(input *[]types.InputCreateOperation, accountID int64, authUserID int64) (*[]*types.Operation, error) {
	if len(*input) > 100 {
		return nil, errors.New("ERROR: too many operations to create")
	}
	var createdOperations []*types.Operation
	for i := range *input {
		if operation, err := rep.Create(&(*input)[i], accountID, authUserID); err != nil {
			log.Print(err)
			continue
		} else {
			createdOperations = append(createdOperations, operation)
		}
	}
	return &createdOperations, nil
}

func (rep *OperationRepository) Create(input *types.InputCreateOperation, accountID int64, authUserID int64) (*types.Operation, error) {

	var operation types.Operation

	var account types.Account
	rep.Db.First(&account, accountID)
	if account.ID == 0 {
		return nil, errors.New(logs.ERRNoSuchToken)
	}

	operation.UID = uuid.Must(uuid.NewV4()).String()
	operation.AccountsID = accountID
	operation.Status = input.Status
	operation.CashType = input.CashType
	operation.Amount = input.Amount
	operation.ToSource = input.ToSource
	operation.ToUID = input.ToUID
	operation.CreateUsersID = authUserID
	if input.Description != nil {
		operation.Description = *input.Description
		if operation.Description == "Пополнение баланса арендатором через личный кабинет (Sberbank)" {
			var tempBillID4 = int64(4)
			input.ToBillsID = &tempBillID4
		}
	}

	//Bills IDs
	if input.ToBillsID != nil {
		var billRep = BillRepository{Db: rep.Db}
		tempBill, err := billRep.GetByID(*input.ToBillsID, account.Token)
		if err != nil {
			return nil, err
		}
		operation.ToBillsID = input.ToBillsID
		if tempBill.CashType != "" {
			operation.CashType = tempBill.CashType
		}
	}
	if input.FromBillsID != nil {
		var billRep = BillRepository{Db: rep.Db}
		if _, err := billRep.GetByID(*input.FromBillsID, account.Token); err != nil {
			return nil, err
		}
		operation.FromBillsID = input.FromBillsID
	}
	if operation.FromBillsID != nil && operation.ToBillsID == nil {
		return nil, errors.New("ERROR: 'to_bills_id' is required when You use 'to_bills_id'")
	}
	if operation.ToBillsID != nil && operation.FromBillsID != nil && *operation.ToBillsID == *operation.FromBillsID {
		return nil, errors.New(logs.ERRSameBillID)
	}

	//From UID And Source
	if input.FromUID != nil {
		operation.FromUID = *input.FromUID
		if operation.FromUID == operation.ToUID {
			return nil, errors.New(logs.ERRSameUID)
		}
	}
	if input.FromSource != nil {
		operation.FromSource = *input.FromSource
	}
	if (operation.FromUID == "" && operation.FromSource != "") || (operation.FromUID != "" && operation.FromSource == "") {
		return nil, errors.New("ERROR: 'from_table' is required when You use 'from_uid'")
	}

	//Other
	if input.ExtID != nil {
		operation.ExtID = *input.ExtID
	}
	if input.CategoriesID != nil {
		if *input.CategoriesID != 0 {
			var category types.Category
			if result := rep.Db.First(&category, "id = ?", *input.CategoriesID); result.RowsAffected != 1 {
				return nil, errors.New(logs.ERRNoSuchCategory)
			}
		}
		operation.CategoriesID = *input.CategoriesID
	}

	if input.Date != nil {
		operation.Date = *input.Date
	}

	if input.RelationsUIDs != nil {
		if input.RelationsSources != nil {
			if len(*input.RelationsUIDs) != len(*input.RelationsSources) {
				return nil, errors.New(logs.ERRRelationsSameElements)
			}
			for i := range *input.RelationsSources {

				if !types.CheckOperationSource((*input.RelationsSources)[i]) {
					return nil, errors.New("ERROR: '" + (*input.RelationsSources)[i] + "' source is not allowed")
				}
				if operation.CategoriesID == 8 && (*input.RelationsSources)[i] == "vehicle" {
					(*input.RelationsSources)[i] = ""
					(*input.RelationsUIDs)[i] = ""
				}
			}
			var tempRelSources []string
			var tempRelUIDs []string
			for i := range *input.RelationsSources {
				if (*input.RelationsSources)[i] != "" && (*input.RelationsUIDs)[i] != "" {
					tempRelSources = append(tempRelSources, (*input.RelationsSources)[i])
					tempRelUIDs = append(tempRelUIDs, (*input.RelationsUIDs)[i])
				}
			}

			tempMarshledUIDs, err := json.Marshal(tempRelUIDs)
			if err != nil {
				return nil, err
			}
			tempUIDsJSON := datatypes.JSON(tempMarshledUIDs)
			operation.RelationsUIDs = tempUIDsJSON

			tempMarshledSources, err := json.Marshal(tempRelSources)
			if err != nil {
				return nil, err
			}
			tempSourcesJSON := datatypes.JSON(tempMarshledSources)
			operation.RelationsSources = tempSourcesJSON
		} else {
			return nil, errors.New("ERROR: 'relations_sources' is required, when you using 'relations_uids'")
		}
	}

	if input.Deposit != nil {
		operation.Deposit = *input.Deposit
		if operation.Deposit && operation.Amount < 0 {
			var totalDeposit float64
			rep.Db.Raw("SELECT COALESCE(SUM (amount), 0) AS total FROM operations WHERE operations.to_uid = ? AND operations.deposit = true AND operations.deleted_at IS NULL AND operations.status != 'FAILED'", operation.ToUID).Scan(&totalDeposit)
			if totalDeposit+operation.Amount < 0 {
				return nil, errors.New(logs.ERRNegativeDeposit)
			}
		}
	}

	if err := rep.checkOperation(&operation); err != nil {
		return nil, err
	}

	var tx1 = rep.Db.Begin()

	var reverseOperation = operation
	if operation.FromBillsID != nil {

		reverseOperation.Amount = operation.Amount * -1
		reverseOperation.UID = uuid.Must(uuid.NewV4()).String()
		reverseOperation.ToBillsID = operation.FromBillsID
		reverseOperation.FromBillsID = operation.ToBillsID

		if result := tx1.Create(&reverseOperation); result.Error != nil {
			err := errors.New("ERROR: Reverse Operation create failed: " + result.Error.Error())
			tx1.Rollback()
			return nil, err
		}

		rep.deleteCacheForOperation(&reverseOperation)

	}

	if result := tx1.Create(&operation); result.Error != nil {
		tx1.Rollback()
		return nil, result.Error
	}
	tx1.Commit()

	rep.deleteCacheForOperation(&operation)

	if err := history.CreateHistory(operation.ID, "operations", "create", nil, &operation, authUserID, accountID); err != nil {
		log.Print("ERROR: " + err.Error())
	}
	if reverseOperation.ID > 0 {
		if err := history.CreateHistory(reverseOperation.ID, "operations", "create", nil, &reverseOperation, authUserID, accountID); err != nil {
			log.Print("ERROR: " + err.Error())
		}
	}

	return &operation, nil
}

func (rep *OperationRepository) Edit(uid string, input *types.InputEditOperation, accountToken string, authUserID int64) (*types.Operation, error) {

	var operation, err = rep.GetByUID(uid, accountToken)
	if err != nil {
		return nil, err
	}

	var dataBefore = *operation

	if input.Status != nil {
		operation.Status = *input.Status
	}
	if input.CashType != nil {
		operation.CashType = *input.CashType
	}
	if input.Amount != nil {
		operation.Amount = *input.Amount
	}
	if input.ToSource != nil {
		operation.ToSource = *input.ToSource
	}
	if input.ToUID != nil {
		operation.ToUID = *input.ToUID
	}

	//Bills IDs
	if input.ToBillsID != nil {
		var billRep = BillRepository{Db: rep.Db}
		_, err := billRep.GetByID(*input.ToBillsID, accountToken)
		if err != nil {
			return nil, err
		}
		operation.ToBillsID = input.ToBillsID
		//if tempBill.CashType != "" {
		//	operation.CashType = tempBill.CashType
		//}
	}
	if input.FromBillsID != nil {
		var billRep = BillRepository{Db: rep.Db}
		if _, err := billRep.GetByID(*input.FromBillsID, accountToken); err != nil {
			return nil, err
		}
		operation.FromBillsID = input.FromBillsID
	}
	if operation.FromBillsID != nil && operation.ToBillsID == nil {
		return nil, errors.New("ERROR: 'to_bills_id' is required when You use 'to_bills_id'")
	}
	if operation.ToBillsID != nil && operation.FromBillsID != nil && *operation.ToBillsID == *operation.FromBillsID {
		return nil, errors.New(logs.ERRSameBillID)
	}

	//From UID And Source
	if input.FromUID != nil {
		operation.FromUID = *input.FromUID
		if operation.FromUID == operation.ToUID {
			return nil, errors.New(logs.ERRSameUID)
		}
	}
	if input.FromSource != nil {
		operation.FromSource = *input.FromSource
	}
	if (operation.FromUID == "" && operation.FromSource != "") || (operation.FromUID != "" && operation.FromSource == "") {
		return nil, errors.New("ERROR: 'from_table' is required when You use 'from_uid'")
	}

	//Other
	if input.ExtID != nil {
		operation.ExtID = *input.ExtID
	}
	if input.CategoriesID != nil {
		if *input.CategoriesID != 0 {
			var category types.Category
			if result := rep.Db.First(&category, "id = ?", *input.CategoriesID); result.RowsAffected != 1 {
				return nil, errors.New(logs.ERRNoSuchCategory)
			}
		}
		operation.CategoriesID = *input.CategoriesID
	}
	if input.Description != nil {
		operation.Description = *input.Description
	}
	if input.Date != nil {
		operation.Date = *input.Date
	}

	if input.Deposit != nil {
		operation.Deposit = *input.Deposit

		var totalDeposit float64
		rep.Db.Raw("SELECT COALESCE(SUM (amount), 0) AS total FROM operations WHERE operations.to_uid = ? AND operations.deposit = true AND operations.deleted_at IS NULL AND operations.status != 'FAILED' AND operations.id != ?", operation.ToUID, operation.ID).Scan(&totalDeposit)
		if operation.Deposit {
			totalDeposit += operation.Amount
		}
		if totalDeposit < 0 {
			return nil, errors.New(logs.ERRNegativeDeposit)
		}

	}

	if input.RelationsUIDs != nil {
		if input.RelationsSources != nil {
			if len(*input.RelationsUIDs) != len(*input.RelationsSources) {
				return nil, errors.New(logs.ERRRelationsSameElements)
			}
			for i := range *input.RelationsSources {

				if !types.CheckOperationSource((*input.RelationsSources)[i]) {
					return nil, errors.New("ERROR: '" + (*input.RelationsSources)[i] + "' source is not allowed")
				}
				if operation.CategoriesID == 8 && (*input.RelationsSources)[i] == "vehicle" {
					(*input.RelationsSources)[i] = ""
					(*input.RelationsUIDs)[i] = ""
				}
			}
			var tempRelSources []string
			var tempRelUIDs []string
			for i := range *input.RelationsSources {
				if (*input.RelationsSources)[i] != "" && (*input.RelationsUIDs)[i] != "" {
					tempRelSources = append(tempRelSources, (*input.RelationsSources)[i])
					tempRelUIDs = append(tempRelUIDs, (*input.RelationsUIDs)[i])
				}
			}

			tempMarshledUIDs, err := json.Marshal(tempRelUIDs)
			if err != nil {
				return nil, err
			}
			tempUIDsJSON := datatypes.JSON(tempMarshledUIDs)
			operation.RelationsUIDs = tempUIDsJSON

			tempMarshledSources, err := json.Marshal(tempRelSources)
			if err != nil {
				return nil, err
			}
			tempSourcesJSON := datatypes.JSON(tempMarshledSources)
			operation.RelationsSources = tempSourcesJSON
		} else {
			return nil, errors.New("ERROR: 'relations_sources' is required, when you using 'relations_uids'")
		}
	}

	if err := rep.checkOperation(operation); err != nil {
		return nil, err
	}

	if result := rep.Db.Save(operation); result.Error != nil {
		return nil, result.Error
	}

	rep.deleteCacheForOperation(operation)

	if err := history.CreateHistory(operation.ID, "operations", "edit", &dataBefore, operation, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return operation, nil
}

//func (rep *OperationRepository) Edit(uid string, input *types.InputEditOperation, accountToken string, authUserID int64) (*types.Operation, error) {
//
//	var operation, err = rep.GetByUID(uid, accountToken)
//	if err != nil {
//		return nil, err
//	}
//
//	var dataBefore = *operation
//
//	if input.Status != nil {
//		operation.Status = *input.Status
//	}
//	if input.CashType != nil {
//		operation.CashType = *input.CashType
//	}
//	if input.Amount != nil {
//		operation.Amount = *input.Amount
//	}
//	if input.ToSource != nil {
//		operation.ToSource = *input.ToSource
//	}
//	if input.ToUID != nil {
//		operation.ToUID = *input.ToUID
//	}
//
//	//Bills IDs
//	if input.ToBillsID != nil {
//		var billRep = BillRepository{Db: rep.Db}
//		if _, err := billRep.GetByID(*input.ToBillsID, accountToken); err != nil {
//			return nil, err
//		}
//		operation.ToBillsID = input.ToBillsID
//	}
//	if input.FromBillsID != nil {
//		var billRep = BillRepository{Db: rep.Db}
//		if _, err := billRep.GetByID(*input.FromBillsID, accountToken); err != nil {
//			return nil, err
//		}
//		operation.FromBillsID = input.FromBillsID
//	}
//	if operation.FromBillsID != nil && operation.ToBillsID == nil {
//		return nil, errors.New("ERROR: 'to_bills_id' is required when You use 'to_bills_id'")
//	}
//	if operation.ToBillsID != nil && operation.FromBillsID != nil && *operation.ToBillsID == *operation.FromBillsID {
//		return nil, errors.New(logs.ERRSameBillID)
//	}
//
//	//From UID And Source
//	if input.FromUID != nil {
//		operation.FromUID = *input.FromUID
//		if operation.FromUID == operation.ToUID {
//			return nil, errors.New(logs.ERRSameUID)
//		}
//	}
//	if input.FromSource != nil {
//		operation.FromSource = *input.FromSource
//	}
//	if (operation.FromUID == "" && operation.FromSource != "") || (operation.FromUID != "" && operation.FromSource == "") {
//		return nil, errors.New("ERROR: 'from_table' is required when You use 'from_uid'")
//	}
//
//	//Other
//	if input.ExtID != nil {
//		operation.ExtID = *input.ExtID
//	}
//	if input.CategoriesID != nil {
//		if *input.CategoriesID != 0 {
//			var category types.Category
//			if result := rep.Db.First(&category, "id = ?", *input.CategoriesID); result.RowsAffected != 1 {
//				return nil, errors.New(logs.ERRNoSuchCategory)
//			}
//		}
//		operation.CategoriesID = *input.CategoriesID
//	}
//	if input.Description != nil {
//		operation.Description = *input.Description
//	}
//	if input.Date != nil {
//		operation.Date = *input.Date
//	}
//
//	if input.Deposit != nil {
//		operation.Deposit = *input.Deposit
//
//		var totalDeposit float64
//		rep.Db.Raw("SELECT COALESCE(SUM (amount), 0) AS total FROM operations WHERE operations.to_uid = ? AND operations.deposit = true AND operations.deleted_at IS NULL AND operations.status != 'FAILED' AND operations.id != ?", operation.ToUID, operation.ID).Scan(&totalDeposit)
//		if operation.Deposit {
//			totalDeposit += operation.Amount
//		}
//		if totalDeposit < 0 {
//			return nil, errors.New(logs.ERRNegativeDeposit)
//		}
//
//	}
//
//	if input.RelationsUIDs != nil {
//		if input.RelationsSources != nil {
//			if len(*input.RelationsUIDs) != len(*input.RelationsSources) {
//				return nil, errors.New(logs.ERRRelationsSameElements)
//			}
//			for i := range *input.RelationsSources {
//
//				if !types.CheckOperationSource((*input.RelationsSources)[i]) {
//					return nil, errors.New("ERROR: '" + (*input.RelationsSources)[i] + "' source is not allowed")
//				}
//				if operation.CategoriesID == 8 && (*input.RelationsSources)[i] == "vehicle" {
//					(*input.RelationsSources)[i] = ""
//					(*input.RelationsUIDs)[i] = ""
//				}
//			}
//			var tempRelSources []string
//			var tempRelUIDs []string
//			for i := range *input.RelationsSources {
//				if (*input.RelationsSources)[i] != "" && (*input.RelationsUIDs)[i] != "" {
//					tempRelSources = append(tempRelSources, (*input.RelationsSources)[i])
//					tempRelUIDs = append(tempRelUIDs, (*input.RelationsUIDs)[i])
//				}
//			}
//
//			tempMarshledUIDs, err := json.Marshal(tempRelUIDs)
//			if err != nil {
//				return nil, err
//			}
//			tempUIDsJSON := datatypes.JSON(tempMarshledUIDs)
//			operation.RelationsUIDs = tempUIDsJSON
//
//			tempMarshledSources, err := json.Marshal(tempRelSources)
//			if err != nil {
//				return nil, err
//			}
//			tempSourcesJSON := datatypes.JSON(tempMarshledSources)
//			operation.RelationsSources = tempSourcesJSON
//		} else {
//			return nil, errors.New("ERROR: 'relations_sources' is required, when you using 'relations_uids'")
//		}
//	}
//
//	if err := rep.checkOperation(operation); err != nil {
//		return nil, err
//	}
//
//	if result := rep.Db.Save(&operation); result.Error != nil {
//		return nil, result.Error
//	}
//
//	if db.RedisDB != nil {
//		if operation.ToBillsID != nil {
//			db.RedisDB.Del(db.RedisContext, helpers.GenerateBillBalanceCacheKey(*operation.ToBillsID))
//		}
//		if input.RelationsUIDs != nil {
//			db.RedisDB.Del(db.RedisContext, helpers.GenerateRedisKeyForRexSearch(operation.ToSource, operation.ToUID, *input.RelationsUIDs))
//		} else {
//			db.RedisDB.Del(db.RedisContext, helpers.GenerateRedisKeyForRexSearch(operation.ToSource, operation.ToUID, []string{}))
//		}
//	}
//
//
//	return operation, nil
//}

func (rep *OperationRepository) Delete(UID string, accountToken string, authUserID int64) error {

	data, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return err
	}

	var dataBefore = *data

	if result := rep.Db.Delete(data); result.Error != nil {
		return result.Error
	}

	rep.deleteCacheForOperation(data)

	if err := history.CreateHistory(data.ID, "operations", "delete", &dataBefore, data, authUserID, dataBefore.AccountsID); err != nil {
		log.Print("ERROR: " + err.Error())
	}

	return nil
}

func (rep *OperationRepository) GetByUID(UID string, accountToken string) (*types.Operation, error) {
	var data types.Operation
	if result := helpers.DbAccountLeftJoin("operations", accountToken, rep.Db).Limit(1).Find(&data, "operations.uid = ? AND operations.deleted_at IS NULL", UID); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchOperationUID)
	}
	return &data, nil
}

func (rep *OperationRepository) listGormHelper(input *types.OperationListFilters, accountToken string, summ, abs, addDayToEnd bool, trueTx *gorm.DB) (*gorm.DB, error) {

	if trueTx == nil {
		trueTx = helpers.DbAccountLeftJoin("operations", accountToken, rep.Db.Model(&types.Operation{}))
	} else {
		trueTx = helpers.DbAccountLeftJoin("operations", accountToken, trueTx)
	}

	if summ {
		if abs {
			if input != nil && input.GroupBy != nil {
				trueTx = trueTx.Select("operations." + *input.GroupBy + ", ABS(COALESCE(SUM (operations.amount), 0))")
			} else {
				trueTx = trueTx.Select("ABS(COALESCE(SUM (operations.amount), 0))")
			}
		} else {
			if input != nil && input.GroupBy != nil {
				trueTx = trueTx.Select("operations." + *input.GroupBy + ", COALESCE(SUM (operations.amount), 0)")
			} else {
				trueTx = trueTx.Select("COALESCE(SUM (operations.amount), 0)")
			}
		}
	}

	if input != nil {
		if input.SearchUID != nil {
			trueTx = trueTx.Where("(operations.relations_uids::jsonb @> ? OR operations.to_uid ILIKE ? OR operations.from_uid ILIKE ?)", "\""+*input.SearchUID+"\"", "%"+*input.SearchUID+"%", "%"+*input.SearchUID+"%")
		} else {
			if input.ToUID != nil {
				trueTx = trueTx.Where("operations.to_uid ILIKE ?", "%"+*input.ToUID+"%")
			}
			if input.InToUIDs != nil {
				trueTx = trueTx.Where("operations.to_uid IN ?", *input.InToUIDs)
			}
			if input.OrRelationsUIDs != nil {
				tx2 := rep.Db
				for i := range *input.OrRelationsUIDs {
					tx2 = tx2.Or("operations.relations_uids::jsonb @> ?", "\""+(*input.OrRelationsUIDs)[i]+"\"")
				}
				trueTx = trueTx.Where(tx2)
			}

			if input.ToUIDAndRelations != nil {
				trueTx = trueTx.Where("(operations.relations_uids::jsonb @> ? OR operations.to_uid = ?)", "\""+*input.ToUIDAndRelations+"\"", *input.ToUIDAndRelations)
			}
			if input.RelationsUIDs != nil {
				for _, uid := range *input.RelationsUIDs {
					uid = "\"" + uid + "\""
					trueTx = trueTx.Where("operations.relations_uids::jsonb @> ?", uid)
				}
			}
		}

		if input.CategoriesID != nil {
			trueTx = trueTx.Where("operations.categories_id = ?", *input.CategoriesID)
		}

		if input.InCategoriesID != nil {
			trueTx = trueTx.Where("operations.categories_id IN ?", *input.InCategoriesID)
		}

		if input.DateStart != nil {
			dateStart, err := makeTimeFromString(*input.DateStart)
			if err != nil {
				return nil, err
			}
			trueTx = trueTx.Where("operations.date >= ?", dateStart)
		}

		if input.DateEnd != nil {
			dateEnd, err := makeTimeFromString(*input.DateEnd)
			if err != nil {
				return nil, err
			}
			if addDayToEnd {
				dateEnd = dateEnd.Add(time.Hour * 24)
			}
			trueTx = trueTx.Where("operations.date < ?", dateEnd)
		}

		if input.CreatedAtStart != nil {
			dateStart, err := makeTimeFromString(*input.CreatedAtStart)
			if err != nil {
				return nil, err
			}
			trueTx = trueTx.Where("operations.created_at >= ?", dateStart)
		}

		if input.CreatedAtEnd != nil {
			dateEnd, err := makeTimeFromString(*input.CreatedAtEnd)
			if err != nil {
				return nil, err
			}
			if addDayToEnd {
				dateEnd = dateEnd.Add(time.Hour * 24)
			}
			trueTx = trueTx.Where("operations.created_at < ?", dateEnd)
		}

		if input.Deposit != nil {
			trueTx = trueTx.Where("operations.deposit = ?", *input.Deposit)
		}

		if input.WithoutCategoriesIDs != nil {
			trueTx = trueTx.Where("(operations.categories_id NOT IN (?))", *input.WithoutCategoriesIDs)
		}

		if input.AmountType != nil {
			switch *input.AmountType {
			case "debit":
				trueTx = trueTx.Where("operations.amount > 0")

			case "credit":
				trueTx = trueTx.Where("operations.amount < 0")
			}

		}

		if input.ToSource != nil {
			trueTx = trueTx.Where("operations.to_source = ?", *input.ToSource)
		}

		if input.CashType != nil {
			trueTx = trueTx.Where("operations.cash_type = ?", *input.CashType)
		}

		if input.BillCashType != nil {
			trueTx = trueTx.Joins("LEFT JOIN bills AS b ON operations.to_bills_id = b.id").Where("b.cash_type = ? OR (b.id IS NULL AND operations.cash_type = ?)", *input.BillCashType, *input.BillCashType)
		}

		if input.WithoutCategoriesIDs != nil {
			trueTx = trueTx.Where("(operations.categories_id NOT IN (?))", *input.WithoutCategoriesIDs)
		}

		if input.ToBillsID != nil {
			trueTx = trueTx.Where("operations.to_bills_id = ?", *input.ToBillsID)
		}

		if input.MisleadingDate != nil {
			if *input.MisleadingDate {
				trueTx = trueTx.Where("operations.date::DATE != operations.created_at::DATE")
			} else {
				trueTx = trueTx.Where("operations.date::DATE == operations.created_at::DATE")
			}
		}

		if input.Search != nil {
			var categoriesIDs []uint64
			db.Db.Table("categories").Select("categories.id").Find(&categoriesIDs, "categories.name ILIKE ?", "%"+*input.Search+"%")
			if len(categoriesIDs) > 0 {
				trueTx = trueTx.Where("(operations.categories_id IN ? OR operations.cash_type ILIKE ?)", categoriesIDs, "%"+*input.Search+"%")
			} else {
				trueTx = trueTx.Where("operations.cash_type ILIKE ?", "%"+*input.Search+"%")
			}

		}
	}
	trueTx = trueTx.Where("operations.deleted_at IS NULL AND operations.status != 'FAILED'")

	if input != nil && input.GroupBy != nil {
		trueTx = trueTx.Group("operations." + *input.GroupBy)
	}

	return trueTx, nil
}

func makeTimeFromString(timeString string) (time.Time, error) {
	var err error
	var formatedTime time.Time
	if formatedTime, err = time.Parse("2006-01-02", timeString); err != nil {
		if formatedTime, err = time.Parse("02-01-2006", timeString); err != nil {
			if formatedTime, err = time.Parse("02.01.2006", timeString); err != nil {
				if formatedTime, err = time.Parse("2006-01-02T15:04:05Z07:00", timeString); err != nil {
					return formatedTime, err
				}
			}
		}
	}

	return formatedTime, nil
}

func (rep *OperationRepository) List(input *types.InputGetListOperations, accountToken string) (*helpers.Pagination, error) {

	//Total Amount
	var totalAmount float64
	txAmount, err := rep.listGormHelper(&input.Filters, accountToken, true, false, true, nil)
	if err != nil {
		return nil, err
	}

	if result := txAmount.Scan(&totalAmount); result.Error != nil {
		return nil, result.Error
	}

	//Total
	var totalObjects int64
	tx, err := rep.listGormHelper(&input.Filters, accountToken, false, false, true, nil)
	if err != nil {
		return nil, err
	}
	tx = tx.Session(&gorm.Session{})

	if result := tx.Count(&totalObjects); result.Error != nil {
		return nil, result.Error
	}

	//List
	tx = helpers.DbSetOrder("operations", input.OrderBy, input.OrderDir)(tx)

	var data []types.Operation
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 0)(tx).Find(&data); result.Error != nil {
		return nil, result.Error
	}

	var categories []types.Category
	insertedCategories := make(map[int64]bool)
	for i := range data {
		if data[i].CategoriesID != 0 {
			var tempCat types.Category
			if _, ok := insertedCategories[data[i].CategoriesID]; !ok {
				if err := rep.Db.First(&tempCat, "id = ?", data[i].CategoriesID).Error; err == nil {
					categories = append(categories, tempCat)
					insertedCategories[data[i].CategoriesID] = true
				}
			}
		}
	}

	var saldoStart = float64(0)
	var saldoEnd = float64(0)
	var saldoInput = input.Filters
	saldoInput.DateStart = nil
	if input.Filters.DateStart != nil {
		saldoInput.DateEnd = input.Filters.DateStart
		if txSaldo, err := rep.listGormHelper(&saldoInput, accountToken, true, false, false, nil); err == nil {
			if result := txSaldo.Scan(&saldoStart); result.Error != nil {
				return nil, result.Error
			}
		} else {
			return nil, err
		}
	}

	if input.Filters.DateEnd != nil {
		saldoInput.DateEnd = input.Filters.DateEnd
		if txSaldo, err := rep.listGormHelper(&saldoInput, accountToken, true, false, true, nil); err == nil {
			if result := txSaldo.Scan(&saldoEnd); result.Error != nil {
				return nil, result.Error
			}
		} else {
			return nil, err
		}
	}

	var totalPages = int(math.Ceil(float64(totalObjects) / float64(input.PageSize)))

	return &helpers.Pagination{Page: input.Page, Items: &data, TotalItems: totalObjects, TotalPages: totalPages, AdditionalData: map[string]interface{}{"categories": &categories, "total_amount": totalAmount, "saldo_start": saldoStart, "saldo_end": saldoEnd}}, nil

}

//func (rep *OperationRepository) ListWithSaldo(input *types.InputGetListOperations, accountToken string) (*helpers.Pagination, error) {
//
//	//Total Amount
//	var totalAmount float64
//	txAmount, err := rep.listGormHelper(&input.Filters, accountToken, true, false, true, nil)
//	if err != nil {
//		return nil, err
//	}
//
//	if result := txAmount.Scan(&totalAmount); result.Error != nil {
//		return nil, result.Error
//	}
//
//	//Total
//	var totalObjects int64
//	tx, err := rep.listGormHelper(&input.Filters, accountToken, false, false, true, nil)
//	if err != nil {
//		return nil, err
//	}
//	tx = tx.Session(&gorm.Session{})
//
//	if result := tx.Count(&totalObjects); result.Error != nil {
//		return nil, result.Error
//	}
//
//	//"SELECT *, sum(operations.amount) OVER (PARTITION BY operations.to_uid ORDER BY operations.date) AS saldo FROM operations"
//
//	//List
//	tx = helpers.DbSetOrder("operations", input.OrderBy, input.OrderDir)(tx)
//
//	var data []types.Operation
//	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 30)(tx).Find(&data); result.Error != nil {
//		return nil, result.Error
//	}
//
//	var categories []types.Category
//	insertedCategories := make(map[int64]bool)
//	for i := range data {
//		if data[i].CategoriesID != 0 {
//			var tempCat types.Category
//			if _, ok := insertedCategories[data[i].CategoriesID]; !ok {
//				if err := rep.Db.First(&tempCat, "id = ?", data[i].CategoriesID).Error; err == nil {
//					categories = append(categories, tempCat)
//					insertedCategories[data[i].CategoriesID] = true
//				}
//			}
//		}
//	}
//
//	var totalPages = int(math.Ceil(float64(totalObjects) / float64(input.PageSize)))
//
//	return &helpers.Pagination{Page: input.Page, Items: &data, TotalItems: totalObjects, TotalPages: totalPages, AdditionalData: map[string]interface{}{"categories": &categories}}, nil
//}

func (rep *OperationRepository) BalanceByDaysForEachBill(input *types.InputBillBalanceByDays, accountToken string) (*map[int64]map[string]types.BillByDay, error) {
	if input.DateStart == nil {
		return nil, errors.New("ERROR: no date_start")
	}
	if input.DateEnd == nil {
		return nil, errors.New("ERROR: no date_end")
	}
	startTime, err := makeTimeFromString(*input.DateStart)
	if err != nil {
		return nil, err
	}
	endTime, err := makeTimeFromString(*input.DateEnd)
	if err != nil {
		return nil, err
	}
	if endTime.Sub(startTime) > time.Hour*24*31*3 {
		return nil, errors.New("ERROR: time difference is longer than 3 months")
	}
	startTimeWithDay := startTime.Add(time.Hour * 24)
	endTimeWithDay := endTime.Add(time.Hour * 24)

	var startingBalances []map[string]interface{}

	if result := helpers.DbAccountLeftJoin("operations", accountToken, rep.Db.Model(&types.Operation{})).Select("operations.to_bills_id, COALESCE(SUM (operations.amount), 0) as amount").Where("operations.to_bills_id IS NOT NULL AND operations.to_bills_id != 0").Where("operations.date < ? AND operations.deleted_at IS NULL", startTimeWithDay).Group("operations.to_bills_id").Scan(&startingBalances); result.Error != nil {
		return nil, result.Error
	}

	var afterStartOperations []types.Operation

	if result := helpers.DbAccountLeftJoin("operations", accountToken, rep.Db.Model(&types.Operation{})).Where("operations.to_bills_id IS NOT NULL AND operations.to_bills_id != 0").Where("operations.date >= ? AND operations.date < ?  AND operations.deleted_at IS NULL", startTimeWithDay, endTimeWithDay).Find(&afterStartOperations); result.Error != nil {
		return nil, result.Error
	}

	var byDaysMap = make(map[int64]map[string]types.BillByDay)
	var StartTimeStringForMap = startTime.Format("2006-01-02")
	for i := range startingBalances {
		byDaysMap[startingBalances[i]["to_bills_id"].(int64)] = make(map[string]types.BillByDay)
		var tempBill = types.BillByDay{Amount: startingBalances[i]["amount"].(float64), MisleadingOperations: 0}
		byDaysMap[startingBalances[i]["to_bills_id"].(int64)][StartTimeStringForMap] = tempBill
	}

	for i := range afterStartOperations {
		if afterStartOperations[i].ToBillsID != nil {
			if _, ok := byDaysMap[*afterStartOperations[i].ToBillsID]; !ok {
				byDaysMap[*afterStartOperations[i].ToBillsID] = make(map[string]types.BillByDay)
			}
			timeString := afterStartOperations[i].Date.Format("2006-01-02")
			createdAtString := afterStartOperations[i].CreatedAt.Format("2006-01-02")
			var tempBill = byDaysMap[*afterStartOperations[i].ToBillsID][timeString]
			if strings.Compare(createdAtString, timeString) != 0 {
				var tempBillAtCreatedAt = byDaysMap[*afterStartOperations[i].ToBillsID][createdAtString]
				tempBillAtCreatedAt.MisleadingOperations++
				byDaysMap[*afterStartOperations[i].ToBillsID][createdAtString] = tempBillAtCreatedAt
			}
			tempBill.Amount += afterStartOperations[i].Amount
			byDaysMap[*afterStartOperations[i].ToBillsID][timeString] = tempBill
		}
	}

	for eachDayTime := startTime; eachDayTime.Before(endTime); eachDayTime = eachDayTime.Add(time.Hour * 24) {
		currentDayString := eachDayTime.Format("2006-01-02")
		nextDayString := eachDayTime.Add(time.Hour * 24).Format("2006-01-02")
		for i := range byDaysMap {
			var tempBill = byDaysMap[i][nextDayString]
			tempBill.Amount += byDaysMap[i][currentDayString].Amount
			byDaysMap[i][nextDayString] = tempBill
		}
	}

	return &byDaysMap, nil

}

func (rep *OperationRepository) HasOperations(input *types.OperationListFilters, accountToken string) (*int64, error) {

	var totalOperations int64
	tx, err := rep.listGormHelper(input, accountToken, false, false, true, nil)
	if err != nil {
		return nil, err
	}

	if result := tx.Count(&totalOperations); result.Error != nil {
		return nil, result.Error
	}

	return &totalOperations, nil
}

// Подсчет данных о балансе, депозите и днях просрочки
func (rep *OperationRepository) AmountAndDaysOfDelay(uid string, accountToken string) *types.MiniBill {
	var miniBill types.MiniBill
	var tempOperations []types.Operation
	tx, err := rep.listGormHelper(nil, accountToken, true, true, true, nil)
	if err != nil {
		return nil
	}
	tx.Where("to_uid = ? AND deposit = false AND categories_id = 8", uid).Scan(&miniBill.DayPaymentsAmount)
	helpers.DbAccountLeftJoin("operations", accountToken, rep.Db).Where("(relations_uids::jsonb @> ? OR to_uid = ?) AND operations.deleted_at IS NULL AND operations.status != 'FAILED'", "\""+uid+"\"", uid).Order("date ASC").Find(&tempOperations)
	for i := range tempOperations {
		if !tempOperations[i].Deposit {
			miniBill.Amount += tempOperations[i].Amount
			if miniBill.Amount < 0 {
				if i == 0 {
					miniBill.DaysOfDelay++
				} else if tempOperations[i].Date.Day() != tempOperations[i-1].Date.Day() {
					miniBill.DaysOfDelay++
				}
			}
		} else {
			miniBill.Deposit += tempOperations[i].Amount
		}
	}
	return &miniBill
}

func (rep *OperationRepository) Summ(accountID int64, accountToken string, input *types.OperationListFilters) (*float64, error) {
	var amount float64

	//redisKey, err := tools.MakeRedisKeySummAmount(accountID, input)
	//if err != nil {
	//	return nil, err
	//}
	//
	//if db.RedisDB != nil {
	//	val, err := db.RedisDB.Get(db.RedisContext, redisKey).Float64()
	//
	//	if (input.WithoutCache != nil && *input.WithoutCache) || err != nil {
	tx, err := rep.listGormHelper(input, accountToken, true, false, true, nil)
	if err != nil {
		return nil, err
	}
	if result := tx.Scan(&amount); result.Error != nil {
		return nil, result.Error
	}
	//		db.RedisDB.Set(db.RedisContext, redisKey, amount, redisTimeExpire)
	//	} else {
	//		amount = val
	//	}
	//	return &amount, nil
	//
	//} else {
	//	return nil, errors.New(logs.ERRRedisNotInited)
	//}
	return &amount, nil
}

func (rep *OperationRepository) BalanceByMonths(accountID int64, accountToken string, input *types.OperationListFilters) (*map[string]float64, error) {

	if input.DateStart == nil || input.DateEnd == nil || input.ToUID == nil {
		return nil, errors.New("ERROR: not enouth data to calculate")
	}

	var err error
	var timeStart, timeEnd time.Time
	if timeStart, err = time.Parse("2006-01", *input.DateStart); err != nil {
		return nil, err
	}
	if timeEnd, err = time.Parse("2006-01", *input.DateEnd); err != nil {
		return nil, err
	}

	var monthsBalances = make(map[string]float64)

	tx, err := rep.listGormHelper(input, accountToken, false, false, true, nil)
	if err != nil {
		return nil, err
	}

	var allSelectedOperations []types.Operation
	if result := tx.Find(&allSelectedOperations); result.Error != nil {
		return nil, result.Error
	}

	for i := range allSelectedOperations {
		monthsBalances[allSelectedOperations[i].Date.Format("2006-01")] += allSelectedOperations[i].Amount
	}

	var lastBalance float64
	for !timeStart.After(timeEnd) {
		var formaterStartString = timeStart.Format("2006-01")
		if _, ok := monthsBalances[formaterStartString]; !ok {
			monthsBalances[formaterStartString] = 0.0
		}
		lastBalance += monthsBalances[formaterStartString]
		monthsBalances[formaterStartString] = lastBalance

		if timeStart.Month() > 11 {
			timeStart = time.Date(timeStart.Year()+1, 1, timeStart.Day(), timeStart.Hour(), timeStart.Minute(), timeStart.Second(), timeStart.Nanosecond(), timeStart.Location())
		} else {
			timeStart = time.Date(timeStart.Year(), timeStart.Month()+1, timeStart.Day(), timeStart.Hour(), timeStart.Minute(), timeStart.Second(), timeStart.Nanosecond(), timeStart.Location())
		}
	}

	monthsBalances["total"] = lastBalance
	//var arrayOfBalancesForSortingAndSumm []types.BalanaceAndMonth
	//for i := range monthsBalances {
	//	arrayOfBalancesForSortingAndSumm = append(arrayOfBalancesForSortingAndSumm, types.BalanaceAndMonth{Balance: monthsBalances[i], Month: i})
	//}
	//
	//sort.Slice(arrayOfBalancesForSortingAndSumm, func(i, j int) bool {
	//	return arrayOfBalancesForSortingAndSumm[i].Month < arrayOfBalancesForSortingAndSumm[j].Month
	//})
	//
	//if len(arrayOfBalancesForSortingAndSumm) > 1 {
	//	for i := 0; i < (len(arrayOfBalancesForSortingAndSumm) - 1); i++ {
	//
	//	}
	//}

	return &monthsBalances, nil
}

func (rep *OperationRepository) Analytics(accountID int64, accountToken string, input *types.OperationListFilters) (*types.Analytics, error) {

	var err error
	var data types.Analytics
	var tempFalse = false

	var dateStart time.Time
	var dateEnd time.Time

	if input.DateStart != nil {
		dateStart, err = time.Parse("2006-01-02", *input.DateStart)
		if err != nil {
			return nil, err
		}
	}

	if input.DateEnd != nil {
		dateEnd, err = time.Parse("2006-01-02", *input.DateEnd)
		if err != nil {
			return nil, err
		}
	}

	input.Deposit = &tempFalse

	var tempSearchAllOperations []types.Operation
	var tempSubAnalytics types.SubAnalytics

	//"SUM (amount) AS full_balance,
	//SUM (CASE WHEN amount < 0 THEN amount ELSE 0 END) AS full_credit,
	//SUM (CASE WHEN amount > 0 THEN amount ELSE 0 END) AS full_debit,
	//SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'CASH' THEN amount ELSE 0 END) AS cash_balance,
	//SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'CASH' AND amount < 0 THEN amount ELSE 0 END) AS cash_credit,
	//SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'CASH' AND amount > 0 THEN amount ELSE 0 END) AS cash_debit,
	//SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'SETTLEMENT' THEN amount ELSE 0 END) AS settlement_balance,
	//SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'SETTLEMENT' AND amount < 0 THEN amount ELSE 0 END) AS settlement_credit,
	//SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'SETTLEMENT' AND amount > 0 THEN amount ELSE 0 END) AS settlement_debit"

	tx := helpers.DbDateTimeIntersect(&dateStart, &dateEnd, rep.Db.Table("operations").Select("SUM (amount) AS full_balance, SUM (CASE WHEN amount < 0 THEN amount ELSE 0 END) AS full_credit, SUM (CASE WHEN amount > 0 THEN amount ELSE 0 END) AS full_debit, SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'CASH' THEN amount ELSE 0 END) AS cash_balance, SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'CASH' AND amount < 0 THEN amount ELSE 0 END) AS cash_credit, SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'CASH' AND amount > 0 THEN amount ELSE 0 END) AS cash_debit, SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'SETTLEMENT' THEN amount ELSE 0 END) AS settlement_balance, SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'SETTLEMENT' AND amount < 0 THEN amount ELSE 0 END) AS settlement_credit, SUM (CASE WHEN COALESCE(b.cash_type, operations.cash_type) = 'SETTLEMENT' AND amount > 0 THEN amount ELSE 0 END) AS settlement_debit, SUM (CASE WHEN operations.categories_id = 8 THEN amount ELSE 0 END) AS day_payments_credit, SUM (CASE WHEN operations.categories_id = 62 THEN amount ELSE 0 END) AS fine_credit, SUM (CASE WHEN operations.categories_id NOT IN (8,62) AND amount < 0 THEN amount ELSE 0 END) AS other_credit"), "date")
	tx = tx.Joins("LEFT JOIN bills AS b ON operations.to_bills_id = b.id")

	tx2 := helpers.DbDateTimeIntersect(&dateStart, &dateEnd, rep.Db.Table("operations"), "date")

	tx, err = rep.listGormHelper(input, accountToken, false, false, true, tx)
	if err != nil {
		return nil, err
	}
	tx2, err = rep.listGormHelper(input, accountToken, false, false, true, tx2)
	if err != nil {
		return nil, err
	}

	if result := tx.Scan(&tempSubAnalytics); result.Error != nil {
		return nil, result.Error
	}
	if result := tx2.Find(&tempSearchAllOperations); result.Error != nil {
		return nil, result.Error
	}

	data.FullBalance = tempSubAnalytics.FullBalance
	data.FullCredit = tempSubAnalytics.FullCredit
	data.FullDebit = tempSubAnalytics.FullDebit

	data.CashBalance = tempSubAnalytics.CashBalance
	data.CashCredit = tempSubAnalytics.CashCredit
	data.CashDebit = tempSubAnalytics.CashDebit

	data.SettlementBalance = tempSubAnalytics.SettlementBalance
	data.SettlementCredit = tempSubAnalytics.SettlementCredit
	data.SettlementDebit = tempSubAnalytics.SettlementDebit

	data.DayPaymentsCredit = tempSubAnalytics.DayPaymentsCredit
	data.FineCredit = tempSubAnalytics.FineCredit
	data.OtherCredit = tempSubAnalytics.OtherCredit

	data.NumberOfOperations = int64(len(tempSearchAllOperations))

	data.BalanceByMonths = make(map[string]types.AnalyticsBalance)
	data.BalanceByDays = make(map[string]types.AnalyticsBalance)

	for i := range tempSearchAllOperations {
		tempDay := tempSearchAllOperations[i].Date.Format("2006-01-02")
		tempMonth := tempSearchAllOperations[i].Date.Format("2006-01")

		if entry, ok := data.BalanceByMonths[tempMonth]; ok {
			if tempSearchAllOperations[i].Amount < 0 {
				entry.Credit += tempSearchAllOperations[i].Amount
			} else {
				entry.Debit += tempSearchAllOperations[i].Amount
			}
			data.BalanceByMonths[tempMonth] = entry
		} else {
			var balance = types.AnalyticsBalance{Credit: 0, Debit: 0}
			if tempSearchAllOperations[i].Amount < 0 {
				balance.Credit += tempSearchAllOperations[i].Amount
			} else {
				balance.Debit += tempSearchAllOperations[i].Amount
			}
			data.BalanceByMonths[tempMonth] = balance
		}

		if entry, ok := data.BalanceByDays[tempDay]; ok {
			if tempSearchAllOperations[i].Amount < 0 {
				entry.Credit += tempSearchAllOperations[i].Amount
			} else {
				entry.Debit += tempSearchAllOperations[i].Amount
			}
			data.BalanceByDays[tempDay] = entry
		} else {
			var balance = types.AnalyticsBalance{Credit: 0, Debit: 0}
			if tempSearchAllOperations[i].Amount < 0 {
				balance.Credit += tempSearchAllOperations[i].Amount
			} else {
				balance.Debit += tempSearchAllOperations[i].Amount
			}
			data.BalanceByDays[tempDay] = balance
		}

	}

	return &data, nil
}

func (rep *OperationRepository) FullAnalytics(accountID int64, accountToken string) (*types.FullAnalytics, error) {

	var mainMap = types.FullAnalytics{Months: make(map[string]types.OneMonthFullAnalytics)}

	//var allCategories []types.Category
	//var mapForCategoriesType = make(map[int64]string)
	//
	//rep.Db.Model(&types.Category{}).Find(&allCategories)
	//for i := range allCategories {
	//	mapForCategoriesType[allCategories[i].ID] = allCategories[i].OperationType
	//}

	var allOperations []types.Operation

	tx2 := rep.Db.Where("operations.accounts_id = ?", accountID).Where("operations.deleted_at IS NULL AND operations.status != 'FAILED' AND deposit = false")
	if result := tx2.Find(&allOperations); result.Error != nil {
		return nil, result.Error
	}

	var totalForAllMonths = types.OneMonthFullAnalytics{Debit: make(map[string]float64), Credit: make(map[string]float64)}

	for i := range allOperations {

		monthTimeString := allOperations[i].Date.Format("2006-01")

		var oneMonth, ok = mainMap.Months[monthTimeString]
		if !ok {
			oneMonth = types.OneMonthFullAnalytics{Debit: make(map[string]float64), Credit: make(map[string]float64)}
		}

		if allOperations[i].Amount < 0 {
			oneMonth.Credit["total"] += allOperations[i].Amount
			oneMonth.Credit[strconv.FormatInt(allOperations[i].CategoriesID, 10)] += allOperations[i].Amount

			totalForAllMonths.Credit["total"] += allOperations[i].Amount
			totalForAllMonths.Credit[strconv.FormatInt(allOperations[i].CategoriesID, 10)] += allOperations[i].Amount
		} else {
			oneMonth.Debit["total"] += allOperations[i].Amount
			oneMonth.Debit[strconv.FormatInt(allOperations[i].CategoriesID, 10)] += allOperations[i].Amount

			totalForAllMonths.Debit["total"] += allOperations[i].Amount
			totalForAllMonths.Debit[strconv.FormatInt(allOperations[i].CategoriesID, 10)] += allOperations[i].Amount
		}

		mainMap.Months[monthTimeString] = oneMonth

		//if categoryType, ok := mapForCategoriesType[allOperations[i].CategoriesID]; ok {
		//	switch categoryType {
		//	case "DEBIT":
		//		oneMonth.Debit[strconv.FormatInt(allOperations[i].CategoriesID, 10)] += allOperations[i].Amount

		//	case "CREDIT":
		//		oneMonth.Credit[strconv.FormatInt(allOperations[i].CategoriesID, 10)] += allOperations[i].Amount
		//	}
		//}

	}

	mainMap.Months["total"] = totalForAllMonths

	return &mainMap, nil
}

func (rep *OperationRepository) VehicleAnalytics(accountID int64, accountToken string, uid string, input *types.InputGetVehicleAnalytics) (*types.VehicleAnalytics, error) {

	var tempFalse = false
	var tempTrue = true
	var output types.VehicleAnalytics

	//Preparing DB functions
	var listInput = types.OperationListFilters{ToUIDAndRelations: &uid, Deposit: nil}

	baseTx, err := rep.listGormHelper(&listInput, accountToken, false, false, true, nil)
	if err != nil {
		return nil, err
	}

	//Deposit true
	listInput.Deposit = &tempTrue
	absSumTrueTx, err := rep.listGormHelper(&listInput, accountToken, true, true, true, nil)
	if err != nil {
		return nil, err
	}

	//Deposit false
	listInput.Deposit = &tempFalse
	sumFalseTx, err := rep.listGormHelper(&listInput, accountToken, true, false, true, nil)
	if err != nil {
		return nil, err
	}
	absSumFalseTx, err := rep.listGormHelper(&listInput, accountToken, true, true, true, nil)
	if err != nil {
		return nil, err
	}

	baseTx = baseTx.Session(&gorm.Session{})
	absSumTrueTx = absSumTrueTx.Session(&gorm.Session{})
	sumFalseTx = sumFalseTx.Session(&gorm.Session{})
	absSumFalseTx = absSumFalseTx.Session(&gorm.Session{})

	//Analytics
	var operation14 types.Operation
	baseTx.Find(&operation14, "categories_id = 14")
	output.VehicleAmount = math.Abs(operation14.Amount)
	output.BuyDate = operation14.Date.GoString()

	absSumTrueTx.Scan(&output.Deposit)

	var tempResult1 float64
	if result := sumFalseTx.Joins("LEFT JOIN categories ON operations.categories_id = categories.id").Where("(operations.categories_id IN (10,48,57) OR categories.parent_id IN (49,54,58))").Scan(&tempResult1); result.Error != nil {
		log.Print(result.Error)
	}

	var tempResult2 float64
	if result := sumFalseTx.Joins("LEFT JOIN categories ON operations.categories_id = categories.id").Where("(operations.categories_id IN (14,16,17,20,21) OR categories.parent_id IN (15,19,23))").Scan(&tempResult2); result.Error != nil {
		log.Print(result.Error)
	}

	output.Result = tempResult1 + tempResult2

	absSumFalseTx.Where("categories_id = 16").Scan(&output.TrackerInstallAmount)
	absSumFalseTx.Where("categories_id = 17").Scan(&output.TrackerMonthAmount)
	absSumFalseTx.Where("categories_id = 20").Scan(&output.RegistrationAmount)
	absSumFalseTx.Where("categories_id = 21").Scan(&output.RepairCreditAmount)
	absSumFalseTx.Where("categories_id = 23").Scan(&output.OtherCreditAmount)
	absSumFalseTx.Where("(categories_id = 14 OR categories_id = 16 OR categories_id = 17 OR categories_id = 20 OR categories_id = 21)").Scan(&output.TotalCreditAmount)
	if input.OrderUIDs != nil {
		rep.Db.Table("operations").Select("ABS(COALESCE(SUM (amount), 0)) AS toooootal").Where("accounts_id = ?", accountID).Where("to_uid IN (?) AND deposit = false AND categories_id = 8", *input.OrderUIDs).Scan(&output.DayPaymentsAmount)
		rep.Db.Raw("SELECT COUNT(*) FROM (SELECT date FROM (SELECT to_char(date, 'DD.MM.YYYY') as date FROM operations WHERE to_uid IN (?) AND categories_id = 8 AND deleted_at IS NULL AND operations.status != 'FAILED') a GROUP BY a.date ORDER BY a.date) b", *input.OrderUIDs).Scan(&output.DaysCount)
	}
	absSumFalseTx.Where("categories_id = 10").Scan(&output.RentDebitAmount)
	absSumFalseTx.Where("categories_id = 48").Scan(&output.InitialPaymentAmount)
	absSumFalseTx.Where("categories_id = 49").Scan(&output.CategoryMandatoryPaymentAmount)
	absSumFalseTx.Where("categories_id = 57").Scan(&output.RepairDebitAmount)
	absSumFalseTx.Where("(categories_id = 10 OR categories_id = 48 OR categories_id = 57)").Scan(&output.TotalDebitAmount)

	if result := absSumFalseTx.Joins("LEFT JOIN categories AS c ON c.id = operations.categories_id").Where("c.parent_id = 15").Scan(&output.ParentsOsagoAndDkAmount); result.Error != nil {
		log.Print(result.Error)
	}
	if result := absSumFalseTx.Joins("LEFT JOIN categories AS c ON c.id = operations.categories_id").Where("(c.parent_id = 15 OR c.parent_id = 19 OR c.parent_id = 23)").Scan(&output.ParentsTotalCreditAmount); result.Error != nil {
		log.Print(result.Error)
	}
	if result := absSumFalseTx.Joins("LEFT JOIN categories AS c ON c.id = operations.categories_id").Where("(c.parent_id = 49 OR c.parent_id = 54)").Scan(&output.ParentsMandatoryPaymentAmount); result.Error != nil {
		log.Print(result.Error)
	}
	if result := absSumFalseTx.Joins("LEFT JOIN categories AS c ON c.id = operations.categories_id").Where("c.parent_id = 58").Scan(&output.ParentsOtherDebitAmount); result.Error != nil {
		log.Print(result.Error)
	}
	if result := absSumFalseTx.Joins("LEFT JOIN categories AS c ON c.id = operations.categories_id").Where("(c.parent_id = 49 OR c.parent_id = 54 OR c.parent_id = 58)").Scan(&output.ParentsTotalDebitAmount); result.Error != nil {
		log.Print(result.Error)
	}

	output.MetaVehicleAmount = math.Abs(output.VehicleAmount) + math.Abs(output.RepairCreditAmount) + math.Abs(output.OtherCreditAmount)

	return &output, nil
}

func (rep *OperationRepository) LastPayment(accountID int64, accountToken string, uid string) (*types.Operation, error) {
	var data types.Operation

	if result := helpers.DbAccountLeftJoin("operations", accountToken, rep.Db).Where("(relations_uids::jsonb @> ? OR to_uid = ?) AND operations.deleted_at IS NULL AND operations.status != 'FAILED'", "\""+uid+"\"", uid).Order("date DESC").First(&data); result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}

func (rep *OperationRepository) CreateFromRex(accountID int64, accountToken string, rexOperation *types.InputCreateFromRex) (*types.Operation, error) {
	if db.VehileDB == nil {
		return nil, errors.New("ERROR: VehicleDB not inited")
	}
	extIDToIDMap := loadUsersIDs()

	var tempCount int64
	db.Db.Unscoped().Table("operations").Where("operations.ext_id = ?", rexOperation.ID).Count(&tempCount)
	if tempCount > 0 {
		log.Print("Operation already exist")
		return nil, errors.New("ERROR: Operation already exist")
	}

	var tempOrderUID, tempUserUID, tempVehicleUID string
	var ok bool

	var existingUser map[string]interface{}
	var existingVehicle map[string]interface{}
	var existingRansomOrder map[string]interface{}

	if rexOperation.RentUserID > 0 {
		var rentUserId int64
		if rexOperation.RentUserID == 97 {
			rentUserId = 53
		} else if rexOperation.RentUserID == 91 {
			rentUserId = 79
		} else if rexOperation.RentUserID == 96 {
			rentUserId = 10
		} else {
			rentUserId = rexOperation.RentUserID
		}

		log.Print("db.VehileDB.Table", &existingUser, rentUserId)
		if result := db.VehileDB.Table("users").Find(&existingUser, "id = ?", rentUserId); result.RowsAffected == 0 {
			log.Print("db.VehileDB.Table result", result)
			log.Print("No such user", rentUserId)
			return nil, errors.New("ERROR: No such user")
		}

		tempUserUID, ok = existingUser["uid"].(string)
		if !ok {
			log.Print("No UID in user")
			return nil, errors.New("ERROR: No UID in user")
		}
	}

	if rexOperation.VehicleID > 0 {
		if result := db.VehileDB.Table("vehicles").Find(&existingVehicle, "id = ?", rexOperation.VehicleID); result.RowsAffected == 0 {
			log.Print("No such vehicle")
			return nil, errors.New("ERROR: No such vehicler")
		}

		tempVehicleUID, ok = existingVehicle["uid"].(string)
		if !ok {
			log.Print("No UID in vehicle")
			return nil, errors.New("ERROR: No UID in vehicle")
		}

		if rexOperation.RentUserID > 0 {
			if result := db.VehileDB.Table("ransom_orders").Find(&existingRansomOrder, "vehicles_id = ? AND users_id = ? AND ((to_char(ransom_orders.created_at, 'YYYY-MM-DD') <= ? OR ransom_orders.contract_date <= ?) AND (contract_date_end IS NULL OR contract_date_end >= ?))", existingVehicle["id"], existingUser["id"], rexOperation.Date, rexOperation.Date, rexOperation.Date); result.RowsAffected == 0 {
				log.Print("existingVehicleId = ", existingVehicle["id"])
				log.Print("existingUserId = ", existingUser["id"])
				log.Print("rexOperationDate = ", rexOperation.Date)
				log.Print("No such ransom_order with vehicles_id = ", existingVehicle["id"])
				return nil, errors.New("ERROR: No such ransom_order with vehicles_id")
			}
			tempOrderUID, ok = existingRansomOrder["uid"].(string)
			if !ok {
				log.Print("No UID in ransom_order")
				return nil, errors.New("ERROR: No UID in ransom_order")
			}
		}
	}
	var tempAuthUserId = int64(0)
	if rexOperation.UserID > 0 {
		tempAuthUserId, _ = (*extIDToIDMap)[rexOperation.UserID]
	}

	//Make input
	var tempInput types.InputCreateOperation

	if tempUserUID == "" && tempVehicleUID != "" {
		tempInput.ToUID = tempVehicleUID
		tempInput.ToSource = "vehicle"
	} else if tempUserUID != "" && tempVehicleUID == "" {
		tempInput.ToUID = tempUserUID
		tempInput.ToSource = "user"
	} else if tempUserUID != "" && tempVehicleUID != "" && tempOrderUID != "" {
		tempInput.ToUID = tempOrderUID
		tempInput.ToSource = "ransom_order"
		tempInput.RelationsUIDs = &[]string{tempUserUID, tempVehicleUID}
		tempInput.RelationsSources = &[]string{"user", "vehicle"}
	} else {
		log.Print("Operation without UIDs ID: " + strconv.FormatInt(rexOperation.ID, 10))
		return nil, errors.New("Operation without UIDs ID: " + strconv.FormatInt(rexOperation.ID, 10))
	}

	var err error
	tempInput.Amount, err = strconv.ParseFloat(rexOperation.Amount, 64)
	if err != nil {
		log.Print(err)
	}
	rexOperation.Date += " +0300"
	tempDate, err := time.Parse("2006-01-02 15:04:05 -0700", rexOperation.Date)
	if err != nil {
		log.Print(err)
	} else {
		tempInput.Date = &tempDate
	}
	tempDate = tempDate.Add(time.Hour * 3)

	tempInput.Description = &rexOperation.Description
	tempInput.Deposit = &rexOperation.Deposit
	tempInput.ExtID = &rexOperation.ID
	tempInput.Status = rexOperation.Status
	tempInput.CategoriesID = &rexOperation.CategoryID
	switch rexOperation.CashType {
	case "cash":
		tempInput.CashType = "CASH"
	default:
		tempInput.CashType = "SETTLEMENT"
	}

	if opera, err := rep.Create(&tempInput, accountID, tempAuthUserId); err != nil {
		log.Print(err)
		return nil, err
	} else {
		if rexOperation.AchiveAt != "" {
			rexOperation.AchiveAt += " +0300"
			if tempAchiveDate, err := time.Parse("2006-01-02 15:04:05 -0700", rexOperation.AchiveAt); err != nil {
				log.Print(err)
			} else {
				if !tempAchiveDate.IsZero() {
					rep.Delete(opera.UID, accountToken, 0)
				}
			}
		}
		return opera, nil
	}
}

func loadUsersIDs() *map[int64]int64 {
	var authUsers []map[string]interface{}
	if db.AuthDB != nil {
		if result := db.AuthDB.Table("users").Find(&authUsers); result.Error != nil {
			log.Print(result.Error)
		}
	}
	var extIDToIDMap = make(map[int64]int64)
	for i := range authUsers {
		if authUsers[i]["id"] != nil && authUsers[i]["ext_id"] != nil {
			extIDToIDMap[authUsers[i]["ext_id"].(int64)] = authUsers[i]["id"].(int64)
		} else {
			log.Print("ERROR: no id or ext_id in authUsers")
		}
	}
	return &extIDToIDMap
}

func (rep *OperationRepository) SummForRansomOrders(accountID int64, accountToken string, input *map[int64][]string) (*map[int64]types.MiniBill, error) {

	var data = make(map[int64]types.MiniBill)
	var tempFalse = false

	absSumFalseTx, err := rep.listGormHelper(&types.OperationListFilters{Deposit: &tempFalse}, accountToken, true, true, true, nil)
	if err != nil {
		return nil, err
	}
	absSumFalseTx = absSumFalseTx.Session(&gorm.Session{})

	for i := range *input {
		if len((*input)[i]) == 2 {
			var miniBill types.MiniBill
			var tempOperations []types.Operation

			absSumFalseTx.Where("(relations_uids::jsonb @> ? AND to_uid = ?) AND categories_id = 8", "\""+(*input)[i][1]+"\"", (*input)[i][0]).Scan(&miniBill.DayPaymentsAmount)
			absSumFalseTx.Where("(relations_uids::jsonb @> ? AND to_uid = ?) AND categories_id = 10", "\""+(*input)[i][1]+"\"", (*input)[i][0]).Scan(&miniBill.RentDebitAmount)
			absSumFalseTx.Where("to_uid = ? AND categories_id = 8", (*input)[i][0]).Scan(&miniBill.Residual)
			helpers.DbAccountLeftJoin("operations", accountToken, rep.Db).Where("(relations_uids::jsonb @> ? AND to_uid = ?) AND operations.deleted_at IS NULL AND operations.status != 'FAILED'", "\""+(*input)[i][1]+"\"", (*input)[i][0]).Order("date ASC").Find(&tempOperations)
			for i := range tempOperations {
				if !tempOperations[i].Deposit {
					miniBill.Amount += tempOperations[i].Amount
					if miniBill.Amount < 0 {
						if i == 0 {
							miniBill.DaysOfDelay++
						} else if tempOperations[i].Date.Day() != tempOperations[i-1].Date.Day() {
							miniBill.DaysOfDelay++
						}
					}
				} else {
					miniBill.Deposit += tempOperations[i].Amount
				}
			}
			data[i] = miniBill
		}
	}

	return &data, nil
}

func (rep *OperationRepository) RecreateOperationsForOrder(accountID int64, input *types.InputRecreateOperationsForOrder, authUserID int64) error {

	startTime, err := time.Parse("2006-01-02T15:04:05Z07:00", input.DateStart)
	if err != nil {
		return err
	}

	var endTime = time.Time{}
	if input.DateEnd != nil {
		if *input.DateEnd != "" {
			endTime, err = time.Parse("2006-01-02T15:04:05Z07:00", *input.DateEnd)
			if err != nil {
				return err
			}
		}
	}

	var tempCategory8 = int64(8)
	var tempRelationUIDs = []string{input.UserUID}
	var tempRelationSourses = []string{"user"}
	var tempInputOperation = types.InputCreateOperation{CategoriesID: &tempCategory8, ToUID: input.OrderUID, ToSource: input.OrderType, Amount: -1 * input.Amount, Status: "DEFAULT", CashType: "SETTLEMENT", Date: &startTime, RelationsUIDs: &tempRelationUIDs, RelationsSources: &tempRelationSourses, Description: &input.Description}

	var tempNow = time.Now()

	if result := rep.Db.Where("to_uid = ? AND relations_uids::jsonb @> ? AND categories_id = 8 AND deleted_at IS NULL", input.OrderUID, "\""+input.UserUID+"\"").Delete(&types.Operation{}); result.Error != nil {
		return result.Error
	}

	switch input.OrderType {
	case "rent_order":
		break
	case "ransom_order":
		startTime = startTime.Add(time.Hour * 24)
	}

	for (startTime.Before(tempNow) || startTime.Equal(tempNow)) && (endTime.IsZero() || startTime.Before(endTime) || startTime.Equal(endTime)) {
		if _, err = rep.Create(&tempInputOperation, accountID, authUserID); err != nil {
			return err
		}
		switch input.OrderType {
		case "rent_order":
			startTime = startTime.Add(MaxDuration(MinDuration(time.Hour*24, endTime.Sub(startTime)), time.Minute))
		case "ransom_order":
			startTime = startTime.Add(time.Hour * 24)
		}

		if input.OrderType == "rent_order" && startTime.Equal(endTime) {
			break
		}

	}

	rep.deleteCache(nil, input.OrderUID, input.OrderType)

	return nil
}

func MaxDuration(fisrt, second time.Duration) time.Duration {
	if fisrt >= second {
		return fisrt
	}
	return second
}

func MinDuration(fisrt, second time.Duration) time.Duration {
	if fisrt <= second {
		return fisrt
	}
	return second
}

func (rep *OperationRepository) History(UID string, accountToken string, input *types.InputGetHistoryList) (*helpers2.Pagination, error) {

	operation, err := rep.GetByUID(UID, accountToken)
	if err != nil {
		return nil, err
	}

	tx := rep.Db.Model(&types.History{}).Where("histories.source_table = ? AND histories.source_id = ?", "operations", operation.ID)

	if input.OperationType != "" {
		tx = tx.Where("histories.operation_type = ?", input.OperationType)
	}

	tx = tx.Session(&gorm.Session{})

	var totalItems int64
	if result := tx.Count(&totalItems); result.Error != nil {
		return nil, result.Error
	}

	tx = helpers.DbSetOrder("operations", input.OrderBy, input.OrderDir)(tx)

	var data []types.History
	if result := helpers.PaginateManual(&input.Page, &input.PageSize, 5, 100)(tx).Scan(&data); result.Error != nil {
		return nil, result.Error
	}

	var tempPages = int(math.Ceil(float64(totalItems) / float64(input.PageSize)))

	return &helpers.Pagination{Page: input.Page, Items: &data, TotalItems: totalItems, TotalPages: tempPages}, nil

}

func (rep *OperationRepository) BalancesGroupedByToUID(accountToken string, input *types.OperationListFilters) (*map[string]float64, error) {

	var balancesMap = make(map[string]float64)

	var tempGroupBy = "to_uid"
	input.GroupBy = &tempGroupBy

	tx, err := rep.listGormHelper(input, accountToken, true, false, true, nil)
	if err != nil {
		return nil, err
	}

	var allBalances []types.MiniOperation
	if result := tx.Scan(&allBalances); result.Error != nil {
		return nil, result.Error
	}

	for i := range allBalances {
		balancesMap[allBalances[i].ToUID] = allBalances[i].Coalesce
	}
	return &balancesMap, nil
}

func (rep *OperationRepository) DeleteOperations8ForOrder(input *types.InputDeleteOperationsForOrder) error {

	if input.CategoriesID != 8 {
		return errors.New("ERROR: wrong input")
	}

	if input.UID == "" {
		return errors.New("ERROR: wrong input")
	}

	if input.RelationsUID == "" {
		return errors.New("ERROR: wrong input")
	}

	if !types.CheckOperationSource(input.ToSource) {
		return errors.New("ERROR: wrong input")
	}

	if result := rep.Db.Where("operations.to_uid = ? AND operations.relations_uids::jsonb @> ? AND operations.to_source = ? AND operations.accounts_id = ? AND operations.categories_id = 8 AND operations.deleted_at IS NULL", input.UID, "\""+input.RelationsUID+"\"", input.ToSource, input.AccountID).Delete(&types.Operation{}); result.Error != nil {
		return result.Error
	}

	return nil
}

func (rep *OperationRepository) CalculateDaysCountForAllUIDs(accountToken string, uids *[]string) (*int64, error) {

	var tempDaysCount int64

	if result := rep.Db.Raw("SELECT COUNT(*) FROM (SELECT date FROM (SELECT to_char(date, 'DD.MM.YYYY') as date FROM operations WHERE to_uid IN (?) AND categories_id = 8 AND deleted_at IS NULL AND operations.status != 'FAILED') a GROUP BY a.date ORDER BY a.date) b", *uids).Scan(&tempDaysCount); result.Error != nil {
		return nil, result.Error
	}

	return &tempDaysCount, nil
}
