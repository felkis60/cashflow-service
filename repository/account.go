package repository

import (
	"errors"

	logs "gitlab.com/felkis60/cashflow-service/log"
	types "gitlab.com/felkis60/cashflow-service/types"

	"gorm.io/gorm"
)

type AccountRepository struct {
	Db *gorm.DB
}

func writeAccount(account *types.Account, input *types.InputCreateEditAccount) error {
	if input.Name != nil {
		account.Name = *input.Name
	}
	if input.Token != nil {
		account.Token = *input.Token
	}
	if account.Name == "" {
		return errors.New(logs.ERRBillNameReq)
	}
	if account.Token == "" {
		return errors.New(logs.ERRTokenReq)
	}
	return nil
}

func (rep *AccountRepository) Create(data *types.Account, input *types.InputCreateEditAccount) error {

	if err := writeAccount(data, input); err != nil {
		return err
	}

	var account types.Account
	if result := rep.Db.First(&account, "token = ?", data.Token); result.RowsAffected != 0 {
		return errors.New(logs.ERRTokenIsOccupied)
	}

	rep.Db.Create(data)

	return nil
}

func (rep *AccountRepository) Edit(data *types.Account, input *types.InputCreateEditAccount, token string) error {

	if input.Token != nil && *input.Token != data.Token {
		var account types.Account
		if result := rep.Db.First(&account, "token = ?", input.Token); result.RowsAffected != 0 {
			return errors.New(logs.ERRTokenIsOccupied)
		}
	}

	writeAccount(data, input)

	rep.Db.Save(data)
	return nil
}

func (rep *AccountRepository) Get(token string) (*types.Account, error) {
	var data types.Account
	if result := rep.Db.First(&data, "token = ?", token); result.RowsAffected == 0 {
		return nil, errors.New(logs.ERRNoSuchToken)
	}
	return &data, nil
}
