package main

import (
	startup "gitlab.com/felkis60/cashflow-service/init"
	"gitlab.com/felkis60/cashflow-service/routines"
)

func main() {
	startup.SystemStartup(true, true, true)
	routines.CalculateBalanceForBills()
}
