package main

import (
	"gitlab.com/felkis60/cashflow-service/database"
	startup "gitlab.com/felkis60/cashflow-service/init"
	"gitlab.com/felkis60/cashflow-service/routines"
)

func main() {
	startup.SystemStartup(true, true, true)
	database.StartVehicles()
	database.StartAuth()
	routines.SyncAll()
}
